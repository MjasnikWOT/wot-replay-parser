package com.mjasnik.wot;

/*
*  WOT stats Excel generation
*
*  @version 1.0 20.05.2021
*  @author  MjasnikWOT
*/

// java standard imports
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.File;
import java.io.FilenameFilter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Arrays;
import java.util.Set;
import java.util.Iterator;
import java.util.Map;

// POI imports
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.ColorScaleFormatting;
import org.apache.poi.ss.usermodel.ConditionalFormattingRule;
import org.apache.poi.ss.usermodel.ConditionalFormattingThreshold.RangeType;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.ExtendedColor;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.SheetConditionalFormatting;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;


import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xddf.usermodel.chart.AxisPosition;
import org.apache.poi.xddf.usermodel.chart.AxisCrosses;
import org.apache.poi.xddf.usermodel.chart.ChartTypes;
import org.apache.poi.xddf.usermodel.chart.LegendPosition;
import org.apache.poi.xddf.usermodel.chart.MarkerStyle;
import org.apache.poi.xddf.usermodel.chart.XDDFCategoryAxis;
import org.apache.poi.xddf.usermodel.chart.XDDFChartLegend;
import org.apache.poi.xddf.usermodel.chart.XDDFDataSource;
import org.apache.poi.xddf.usermodel.chart.XDDFDataSourcesFactory;
import org.apache.poi.xddf.usermodel.chart.XDDFLineChartData;
import org.apache.poi.xddf.usermodel.chart.XDDFNumericalDataSource;
import org.apache.poi.xddf.usermodel.chart.XDDFValueAxis;
import org.apache.poi.xssf.usermodel.XSSFChart;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;

import org.apache.poi.xddf.usermodel.XDDFColor;
import org.apache.poi.xddf.usermodel.XDDFSolidFillProperties;
import org.apache.poi.xddf.usermodel.XDDFShapeProperties;
import org.apache.poi.xddf.usermodel.XDDFLineProperties;
import org.apache.poi.xddf.usermodel.chart.XDDFChartData;

import org.apache.poi.util.Units;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTMarker;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTPlotArea;

// main processing class
public class wotstats
{
	// column locations
	private static HashMap<String, HashMap<String, String>> srcColLocs = new HashMap<String, HashMap<String, String>>();
	// final columns for calculations and graphs
	private static HashMap[] calcMapDet;
	// final columns
	private static String[][] helperCols;
	// graph configuration
	private static HashMap[][] graphConfig;
	// args
	private static HashMap procesArgs;
	// reserverved common
	private static int GRAPH_COMMON = 0;
	// graph types
	private static int GRAPH_TYPE_STATS = 0;
	private static int GRAPH_TYPE_SPREAD = 1;

	// main method to run stats generation
	public static void main(String[] args) throws Exception
	{
		// vars
		ArrayList<String[]> fileContents = null;

		// check arguments
		if (args.length == 0)
		{
			System.out.println("No arguments passed!");
			System.out.println("The usage is:");
			System.out.println("    wotstats --D=x --O=x --T=x");
			System.out.println("        --D=x, x specifies the directory where CSV stats for matches are to be found");
			System.out.println("        --O=x, x specifies y/Y/true/TRUE if XLSX stats files needs to be recreated");
			System.out.println("        --T=x, x specifies simple/SIMPLE to generate simple graphs for battle flow");
		}
		else
		{
			// check args
			String dir = "";
			boolean ow = false;
			boolean simple = false;
			procesArgs = new HashMap();

			// directory
			for(int i = 0; i < args.length; i++)
			{
				// check for dir
				if (args[i].startsWith("--D="))
					dir = args[i].replaceAll("--D=", "");
				if (args[i].startsWith("--O="))
					if ("--O=Y".equals(args[i].toUpperCase()) || "--O=TRUE".equals(args[i].toUpperCase()))
						ow = true;
				if (args[i].startsWith("--T="))
					procesArgs.put("GRTYPE", (("--T=SIMPLE".equals(args[i].toUpperCase())) ? "SIMPLE" : "DEFAULT"));
			}
			if ("".equals(dir))
			{
				System.out.println("Directory not passsed!");
				System.exit(1);
			}

			// directory
			File f = new File(dir);
			// filter setup
			FilenameFilter filter = new FilenameFilter()
			{
				@Override
				public boolean accept(File f, String name)
				{
				// We want to find only .csv files
					return name.endsWith(".csv");
				}
			};

			// instead of String
			File[] files = f.listFiles(filter);
			Arrays.sort(files);
			// Get the names of the files by using the .getName() method
			for (int i = 0; i < files.length; i++)
			{
				// check if excel already exists
				if (new File(files[i].getCanonicalPath().replaceAll(".csv", ".xlsx")).exists() && !ow)
					System.out.println("Excel file for \"" + files[i].getName() + "\" already exists, skipping...");
				else
				{
					System.out.println("Processing: " + files[i].getName());
					// read file contents
					fileContents = readFile(files[i].getCanonicalPath());
					// prepare column locations
					prepareInitialStatsConfig(fileContents.get(0));
					// spread init
					prepareInitialSpreadConfig();
					// calculate final columns
					prepareFinalCalcMaps(fileContents.size()-1);
					// save output file
					processAndSaveRsults(fileContents, files[i].getCanonicalPath().replaceAll(".csv", ".xlsx"));
				}
			}
		}
	}

	// this will prepare column locations of source data (to be used in references)
	// to preserve order and stuff, these are arrays instead of hash or other maps
	private static void prepareInitialStatsConfig(String[] pFirstRow)
	{
		// delete all we have
		srcColLocs.clear();
		// all row cols
		for(int i = 0; i < pFirstRow.length; i++)
		{
			// index and col reference
			HashMap<String, String> tmp = new HashMap<>();
			tmp.put("IDX", String.valueOf(i));
			tmp.put("COL", CellReference.convertNumToColString(i));
			// store indexes of rows
			srcColLocs.put(pFirstRow[i], tmp);
			//System.out.println(pFirstRow[i]);
		}

		// helper cols to prepare calc sheet
		helperCols = new String[][]{
			{"Nr", "CONCATENATE(\"\", _ROW_-1)", null},
			{"Day", "SUBSTITUTE(CONCATENATE(\"D \", MID(stats!$" + srcColLocs.get("Battle date").get("COL") + "_ROW_,6,5)), \"-\", \".\")", null, null},
			{"Session Nr.", "CONCATENATE(\"S \", stats!$" + srcColLocs.get("Session number").get("COL") + "_ROW_)", null, null},
			{"Winrate", "stats!$" + srcColLocs.get("Win").get("COL") + "_ROW_", "%", "#00FF00"},  // FF420E
			{"My hits / shots", "ROUND(IF(stats!$" + srcColLocs.get("My shots").get("COL") + "_ROW_=0,0,stats!$" + srcColLocs.get("My hits").get("COL") + "_ROW_/stats!$" + srcColLocs.get("My shots").get("COL") + "_ROW_),2)", "%", "#FFB66C"},  // AECF00
			{"My pens / hits", "ROUND(IF(stats!$" + srcColLocs.get("My hits").get("COL") + "_ROW_=0,0,stats!$" + srcColLocs.get("My pens").get("COL") + "_ROW_/stats!$" + srcColLocs.get("My hits").get("COL") + "_ROW_),2)", "%", "#FF8000"},  // 579D1C
			{"Team hits / shots", "ROUND(IF(stats!$" + srcColLocs.get("Team shots").get("COL") + "_ROW_=0,0,stats!$" + srcColLocs.get("Team hits").get("COL") + "_ROW_/stats!$" + srcColLocs.get("Team shots").get("COL") + "_ROW_),2)", "%", "#83CAFF"},  // AECF00
			{"Team pens / hits", "ROUND(IF(stats!$" + srcColLocs.get("Team hits").get("COL") + "_ROW_=0,0,stats!$" + srcColLocs.get("Team pens").get("COL") + "_ROW_/stats!$" + srcColLocs.get("Team hits").get("COL") + "_ROW_),2)", "%", "#0084D1"},  // 579D1C
			{"Enemy hits / shots", "ROUND(IF(stats!$" + srcColLocs.get("Enemy shots").get("COL") + "_ROW_=0,0,stats!$" + srcColLocs.get("Enemy hits").get("COL") + "_ROW_/stats!$" + srcColLocs.get("Enemy shots").get("COL") + "_ROW_),2)", "%", "#FFA6A6"},  // AECF00
			{"Enemy pens / hits", "ROUND(IF(stats!$" + srcColLocs.get("Enemy hits").get("COL") + "_ROW_=0,0,stats!$" + srcColLocs.get("Enemy pens").get("COL") + "_ROW_/stats!$" + srcColLocs.get("Enemy hits").get("COL") + "_ROW_),2)", "%", "#FF420E"},  // 579D1C
			{"Battle len", "ROUND(-stats!$" + srcColLocs.get("Battle len").get("COL") + "_ROW_/900,2)", "%", "#999999"},  // 999999
			{"Score diff", "ROUND(stats!$" + srcColLocs.get("Score diff %").get("COL") + "_ROW_,2)", "%", "#B47804"},  // 800080
			{"Prem diff", "ROUND(stats!$" + srcColLocs.get("Prem tank diff %").get("COL") + "_ROW_,2)", "%", "#C5000B"},  // FFD700
			{"Tier diff", "ROUND(stats!$" + srcColLocs.get("Tier diff %").get("COL") + "_ROW_,2)", "%", "#314004"},  // 3465A4
			{"My lifetime", "ROUND(stats!$" + srcColLocs.get("My lifetime").get("COL") + "_ROW_/stats!$" + srcColLocs.get("Battle len").get("COL") + "_ROW_,2)", "%", "#0084D1"},  // 3FAF46
			{"Team AVG lifetime", "ROUND(stats!$" + srcColLocs.get("Team AVG lifetime").get("COL") + "_ROW_/stats!$" + srcColLocs.get("Battle len").get("COL") + "_ROW_,2)", "%", "#AECF00"},  // FFD428
			{"Enemy AVG lifetime", "ROUND(stats!$" + srcColLocs.get("Enemy AVG lifetime").get("COL") + "_ROW_/stats!$" + srcColLocs.get("Battle len").get("COL") + "_ROW_,2)", "%", "#A1467E"},  // 6B5E9B
			{"My survival", "ROUND(stats!$" + srcColLocs.get("My death").get("COL") + "_ROW_,2)", "%", "#579D1C"},  // 00FF00
			{"My XP / team AVG XP", "ROUND(IF(stats!$" + srcColLocs.get("Team AVG XP").get("COL") + "_ROW_=0,0,stats!$" + srcColLocs.get("My XP").get("COL") + "_ROW_/stats!$" + srcColLocs.get("Team AVG XP").get("COL") + "_ROW_),2)", "%", "#83CAFF"},  // FF8000
			{"My DMG / team AVG DMG", "ROUND(IF(stats!$" + srcColLocs.get("Team AVG DMG").get("COL") + "_ROW_=0,0,stats!$" + srcColLocs.get("My total DMG").get("COL") + "_ROW_/stats!$" + srcColLocs.get("Team AVG DMG").get("COL") + "_ROW_),2)", "%", "#7E0021"},  // 8D281E
			{"Prem DMG diff", "ROUND(stats!$" + srcColLocs.get("Prem tank DMG diff %").get("COL") + "_ROW_,2)", "%", "#4B1F6F"},  // FF6D6D
			{"Mileage diff", "ROUND(stats!$" + srcColLocs.get("Mileage diff %").get("COL") + "_ROW_,2)", "%", "#0000FF"}  // 0000FF
		};

		// !!! graph configuration !!! adjust this when adding more graphs !!!
		String[] graphs;
		int[] graphCols;
		if ("SIMPLE".equals((String)procesArgs.get("GRTYPE")))
			graphs = new String[]{"COMMON", "AVG_TOTAL_GAME_SIMPLE", "AVG_TOTAL_TEAMS_SIMPLE", "AVG_SLIDING_N_GAME_SIMPLE", "AVG_SLIDING_N_TEAMS_SIMPLE"};
		else
			 graphs = new String[]{"COMMON", "AVG_TOTAL_GAME", "AVG_TOTAL_MY", "AVG_TOTAL_TEAMS", "AVG_SLIDING_N_GAME"
			 	, "AVG_SLIDING_N_MY", "AVG_SLIDING_N_TEAMS", "AVG_SLIDING_DAY_GAME", "AVG_SLIDING_DAY_MY", "AVG_SLIDING_DAY_TEAMS"
				, "AVG_SLIDING_SESSSION_GAME", "AVG_SLIDING_SESSSION_MY", "AVG_SLIDING_SESSSION_TEAMS"
			};
		// total graph count (0 - common stuff, 1+ - graphs)
		graphConfig = new HashMap[2][graphs.length];
		HashMap graphElem;
		int graphCnt = -1;

		for(int i = 0; i < graphs.length; i++)
		{
			// init graph config
			graphCnt++;
			graphElem = new HashMap();
			// COMMON
			if (graphs[i].equals("COMMON"))
			{
				graphElem.put("CODE", graphs[i]);
				graphElem.put("CALC_SHEET", "calc");
				graphElem.put("FILL_COLOR", "#FFFFFF");
				graphElem.put("GRID_COLOR", "#B3B3B3");
				graphElem.put("LABEL_ROT", (int)Math.round(-5400000/2d)); // -45
				graphElem.put("GRAPH_WIDTH", 32);
				graphElem.put("GRAPH_HEIGHT", 40);
				graphElem.put("GRAPH_START_COL", 0);
			}
			// NEW GRAPH ORGANIZED BY MY/TEAMS STUFF
			// AVG_TOTAL_GAME
			else if (graphs[i].equals("AVG_TOTAL_GAME"))
			{
				graphElem.put("CODE", graphs[i]);
				graphElem.put("TITLE", "AVG (total) game stats");
				graphElem.put("CAT_TITLE", "Game count");
				graphElem.put("VAL_TITLE", "percent (%)");
				graphElem.put("COLUMN_NAMES", new String[]{"Winrate", "Battle len", "Tier diff", "Prem diff", "Score diff"});
				graphElem.put("COLUMN_IDS", new int[]{-1,-1,-1,-1,-1}); // these are to be filed in preparation
			}
			// AVG_TOTAL_MY
			else if (graphs[i].equals("AVG_TOTAL_MY"))
			{
				graphElem.put("CODE", graphs[i]);
				graphElem.put("TITLE", "AVG (total) my stats");
				graphElem.put("CAT_TITLE", "Game count");
				graphElem.put("VAL_TITLE", "percent (%)");
				graphElem.put("COLUMN_NAMES", new String[]{"Winrate", "My survival", "My lifetime", "My hits / shots", "My pens / hits", "My DMG / team AVG DMG", "My XP / team AVG XP"});
				graphElem.put("COLUMN_IDS", new int[]{-1,-1,-1,-1,-1,-1,-1}); // these are to be filed in preparation
			}
			// AVG_TOTAL_TEAMS
			else if (graphs[i].equals("AVG_TOTAL_TEAMS"))
			{
				graphElem.put("CODE", graphs[i]);
				graphElem.put("TITLE", "AVG (total) teams stats");
				graphElem.put("CAT_TITLE", "Game count");
				graphElem.put("VAL_TITLE", "percent (%)");
				graphElem.put("COLUMN_NAMES", new String[]{"Team AVG lifetime", "Enemy AVG lifetime", "Prem DMG diff", "Team hits / shots", "Team pens / hits", "Enemy hits / shots", "Enemy pens / hits"});
				graphElem.put("COLUMN_IDS", new int[]{-1,-1,-1,-1,-1,-1,-1}); // these are to be filed in preparation
			}
			// AVG_SLIDING_N_GAME
			else if (graphs[i].equals("AVG_SLIDING_N_GAME"))
			{
				graphElem.put("CODE", graphs[i]);
				graphElem.put("TITLE", "Rolling (n) AVG game stats");
				graphElem.put("CAT_TITLE", "Game count");
				graphElem.put("VAL_TITLE", "percent (%)");
				graphElem.put("COLUMN_NAMES", new String[]{"Winrate", "Battle len", "Tier diff", "Prem diff", "Score diff"});
				graphElem.put("COLUMN_IDS", new int[]{-1,-1,-1,-1,-1}); // these are to be filed in preparation
			}
			// AVG_SLIDING_N_MY
			else if (graphs[i].equals("AVG_SLIDING_N_MY"))
			{
				graphElem.put("CODE", graphs[i]);
				graphElem.put("TITLE", "Rolling (n) AVG my stats");
				graphElem.put("CAT_TITLE", "Game count");
				graphElem.put("VAL_TITLE", "percent (%)");
				graphElem.put("COLUMN_NAMES", new String[]{"Winrate", "My survival", "My lifetime", "My hits / shots", "My pens / hits", "My DMG / team AVG DMG", "My XP / team AVG XP"});
				graphElem.put("COLUMN_IDS", new int[]{-1,-1,-1,-1,-1,-1,-1}); // these are to be filed in preparation
			}
			// AVG_SLIDING_N_TEAMS
			else if (graphs[i].equals("AVG_SLIDING_N_TEAMS"))
			{
				graphElem.put("CODE", graphs[i]);
				graphElem.put("TITLE", "Rolling (n) AVG teams stats");
				graphElem.put("CAT_TITLE", "Game count");
				graphElem.put("VAL_TITLE", "percent (%)");
				graphElem.put("COLUMN_NAMES", new String[]{"Team AVG lifetime", "Enemy AVG lifetime", "Prem DMG diff", "Team hits / shots", "Team pens / hits", "Enemy hits / shots", "Enemy pens / hits"});
				graphElem.put("COLUMN_IDS", new int[]{-1,-1,-1,-1,-1,-1,-1}); // these are to be filed in preparation
			}
			// AVG_SLIDING_DAY_GAME
			else if (graphs[i].equals("AVG_SLIDING_DAY_GAME"))
			{
				graphElem.put("CODE", graphs[i]);
				graphElem.put("TITLE", "Rolling (day) AVG game stats");
				graphElem.put("CAT_TITLE", "Day (MM.DD)");
				graphElem.put("VAL_TITLE", "percent (%)");
				graphElem.put("COLUMN_NAMES", new String[]{"Winrate", "Battle len", "Tier diff", "Prem diff", "Score diff"});
				graphElem.put("COLUMN_IDS", new int[]{-1,-1,-1,-1,-1}); // these are to be filed in preparation
			}
			// AVG_SLIDING_DAY_MY
			else if (graphs[i].equals("AVG_SLIDING_DAY_MY"))
			{
				graphElem.put("CODE", graphs[i]);
				graphElem.put("TITLE", "Rolling (day) AVG my stats");
				graphElem.put("CAT_TITLE", "Day (MM.DD)");
				graphElem.put("VAL_TITLE", "percent (%)");
				graphElem.put("COLUMN_NAMES", new String[]{"Winrate", "My survival", "My lifetime", "My hits / shots", "My pens / hits", "My DMG / team AVG DMG", "My XP / team AVG XP"});
				graphElem.put("COLUMN_IDS", new int[]{-1,-1,-1,-1,-1,-1,-1}); // these are to be filed in preparation
			}
			// AVG_SLIDING_DAY_TEAMS
			else if (graphs[i].equals("AVG_SLIDING_DAY_TEAMS"))
			{
				graphElem.put("CODE", graphs[i]);
				graphElem.put("TITLE", "Rolling (day) AVG teams stats");
				graphElem.put("CAT_TITLE", "Day (MM.DD)");
				graphElem.put("VAL_TITLE", "percent (%)");
				graphElem.put("COLUMN_NAMES", new String[]{"Team AVG lifetime", "Enemy AVG lifetime", "Prem DMG diff", "Team hits / shots", "Team pens / hits", "Enemy hits / shots", "Enemy pens / hits"});
				graphElem.put("COLUMN_IDS", new int[]{-1,-1,-1,-1,-1,-1,-1}); // these are to be filed in preparation
			}
			// AVG_SLIDING_SESSSION_GAME
			else if (graphs[i].equals("AVG_SLIDING_SESSSION_GAME"))
			{
				graphElem.put("CODE", graphs[i]);
				graphElem.put("TITLE", "Rolling (session) AVG game stats");
				graphElem.put("CAT_TITLE", "Session number");
				graphElem.put("VAL_TITLE", "percent (%)");
				graphElem.put("COLUMN_NAMES", new String[]{"Winrate", "Battle len", "Tier diff", "Prem diff", "Score diff"});
				graphElem.put("COLUMN_IDS", new int[]{-1,-1,-1,-1,-1}); // these are to be filed in preparation
			}
			// AVG_SLIDING_SESSSION_MY
			else if (graphs[i].equals("AVG_SLIDING_SESSSION_MY"))
			{
				graphElem.put("CODE", graphs[i]);
				graphElem.put("TITLE", "Rolling (session) AVG my stats");
				graphElem.put("CAT_TITLE", "Session number");
				graphElem.put("VAL_TITLE", "percent (%)");
				graphElem.put("COLUMN_NAMES", new String[]{"Winrate", "My survival", "My lifetime", "My hits / shots", "My pens / hits", "My DMG / team AVG DMG", "My XP / team AVG XP"});
				graphElem.put("COLUMN_IDS", new int[]{-1,-1,-1,-1,-1,-1,-1}); // these are to be filed in preparation
			}
			// AVG_SLIDING_SESSSION_TEAMS
			else if (graphs[i].equals("AVG_SLIDING_SESSSION_TEAMS"))
			{
				graphElem.put("CODE", graphs[i]);
				graphElem.put("TITLE", "Rolling (session) AVG teams stats");
				graphElem.put("CAT_TITLE", "Session number");
				graphElem.put("VAL_TITLE", "percent (%)");
				graphElem.put("COLUMN_NAMES", new String[]{"Team AVG lifetime", "Enemy AVG lifetime", "Prem DMG diff", "Team hits / shots", "Team pens / hits", "Enemy hits / shots", "Enemy pens / hits"});
				graphElem.put("COLUMN_IDS", new int[]{-1,-1,-1,-1,-1,-1,-1}); // these are to be filed in preparation
			}
			// AVG_TOTAL_GAME_SIMPLE
			else if (graphs[i].equals("AVG_TOTAL_GAME_SIMPLE"))
			{
				graphElem.put("CODE", graphs[i]);
				graphElem.put("TITLE", "AVG (total) game stats");
				graphElem.put("CAT_TITLE", "Game count");
				graphElem.put("VAL_TITLE", "percent (%)");
				graphElem.put("COLUMN_NAMES", new String[]{"Winrate", "My hits / shots", "My pens / hits", "Team hits / shots", "Team pens / hits", "Enemy hits / shots", "Enemy pens / hits"});
				//graphElem.put("COLUMN_IDS", new int[]{-1,-1,-1,-1,-1,-1,-1}); // these are to be filed in preparation
			}
			// AVG_TOTAL_TEAMS_SIMPLE
			else if (graphs[i].equals("AVG_TOTAL_TEAMS_SIMPLE"))
			{
				graphElem.put("CODE", graphs[i]);
				graphElem.put("TITLE", "AVG (total) teams stats");
				graphElem.put("CAT_TITLE", "Game count");
				graphElem.put("VAL_TITLE", "percent (%)");
				graphElem.put("COLUMN_NAMES", new String[]{"Winrate", "Battle len", "Team AVG lifetime", "Enemy AVG lifetime", "Mileage diff", "Tier diff", "Score diff"});
				//graphElem.put("COLUMN_IDS", new int[]{-1,-1,-1,-1,-1,-1,-1}); // these are to be filed in preparation
			}
			// AVG_SLIDING_N_GAME_SIMPLE
			else if (graphs[i].equals("AVG_SLIDING_N_GAME_SIMPLE"))
			{
				graphElem.put("CODE", graphs[i]);
				graphElem.put("TITLE", "Rolling (n) AVG game stats");
				graphElem.put("CAT_TITLE", "Game count");
				graphElem.put("VAL_TITLE", "percent (%)");
				graphElem.put("COLUMN_NAMES", new String[]{"Winrate", "My hits / shots", "My pens / hits", "Team hits / shots", "Team pens / hits", "Enemy hits / shots", "Enemy pens / hits"});
				//graphElem.put("COLUMN_IDS", new int[]{-1,-1,-1,-1,-1,-1,-1}); // these are to be filed in preparation
			}
			// AVG_SLIDING_N_TEAMS_SIMPLE
			else if (graphs[i].equals("AVG_SLIDING_N_TEAMS_SIMPLE"))
			{
				graphElem.put("CODE", graphs[i]);
				graphElem.put("TITLE", "Rolling (n) AVG teams stats");
				graphElem.put("CAT_TITLE", "Game count");
				graphElem.put("VAL_TITLE", "percent (%)");
				graphElem.put("COLUMN_NAMES", new String[]{"Winrate", "Battle len", "Team AVG lifetime", "Enemy AVG lifetime", "Mileage diff", "Tier diff", "Score diff"});
				//graphElem.put("COLUMN_IDS", new int[]{-1,-1,-1,-1,-1,-1,-1}); // these are to be filed in preparation
			}
			else
			{
				System.out.println("ERROR: incompatible graph type " + graphs[i]);
				System.exit(1);
			}

			// final touches
			if (!graphs[i].equals("COMMON"))
			{
				// column count
				graphCols = new int[((String[])graphElem.get("COLUMN_NAMES")).length];
				for(int j = 0; j < graphCols.length; j++)
					graphCols[j] = -1;
				graphElem.put("COLUMN_IDS", graphCols);
				// first row
				graphElem.put("FIRST_ROW", 1);
				 // these are to be filed in preparation
				graphElem.put("CAT_COL_ID", -1);
				graphElem.put("LAST_ROW", -1);
			}
			// add config
			graphConfig[GRAPH_TYPE_STATS][graphCnt] = graphElem;
		}
	}

	private static void prepareInitialSpreadConfig()
	{
		// variables
		HashMap graphElem;
		int graphCnt = -1;

		// init graph config
		graphCnt++;
		graphElem = new HashMap();
		// COMMON
		graphElem.put("CODE", "COMMON");
		graphElem.put("CALC_SHEET", "spread");
		graphElem.put("FILL_COLOR", "#FFFFFF");
		graphElem.put("GRID_COLOR", "#B3B3B3");
		graphElem.put("LABEL_ROT", (int)Math.round(-5400000/2d)); // -45
		graphElem.put("GRAPH_WIDTH", 17);
		graphElem.put("GRAPH_HEIGHT", 15);
		graphElem.put("GRAPH_START_COL", 6);
		// add config
		graphConfig[GRAPH_TYPE_SPREAD][graphCnt] = graphElem;

		// init graph config
		graphCnt++;
		graphElem = new HashMap();
		// Tier spread
		graphElem.put("CODE", "TIER_SPREAD");
		graphElem.put("TITLE", "Tier spread");
		graphElem.put("CAT_TITLE", "Tier");
		graphElem.put("VAL_TITLE", "percent (%)");
		graphElem.put("COLUMN_NAMES", new String[]{"Tier spread %"});
		graphElem.put("COLUMN_IDS", new int[]{2});
		graphElem.put("FIRST_ROW", 1);
		graphElem.put("CAT_COL_ID", 0);
		graphElem.put("LAST_ROW", 5);
		graphElem.put("SER_COLOR", "#FFBF00");
		// add config
		graphConfig[GRAPH_TYPE_SPREAD][graphCnt] = graphElem;

		// init graph config
		graphCnt++;
		graphElem = new HashMap();
		// Battle spread
		graphElem.put("CODE","BATTLE_SPREAD");
		graphElem.put("TITLE", "Battle len spread");
		graphElem.put("CAT_TITLE", "Battle length (min)");
		graphElem.put("VAL_TITLE", "percent (%)");
		graphElem.put("COLUMN_NAMES", new String[]{"Battle len spread %"});
		graphElem.put("COLUMN_IDS", new int[]{2});
		graphElem.put("FIRST_ROW", 8);
		graphElem.put("CAT_COL_ID", 0);
		graphElem.put("LAST_ROW", 22);
		graphElem.put("SER_COLOR", "#FFBF00");
		// add config
		graphConfig[GRAPH_TYPE_SPREAD][graphCnt] = graphElem;

		// init graph config
		graphCnt++;
		graphElem = new HashMap();
		// Score spread
		graphElem.put("CODE", "SCORE_SPREAD");
		graphElem.put("TITLE", "Score spread");
		graphElem.put("CAT_TITLE", "Score diff");
		graphElem.put("VAL_TITLE", "percent (%)");
		graphElem.put("COLUMN_NAMES", new String[]{"Score diff spread %"});
		graphElem.put("COLUMN_IDS", new int[]{2});
		graphElem.put("FIRST_ROW", 25);
		graphElem.put("CAT_COL_ID", 0);
		graphElem.put("LAST_ROW", 40);
		graphElem.put("SER_COLOR", "#FFBF00");
		// add config
		graphConfig[GRAPH_TYPE_SPREAD][graphCnt] = graphElem;
	}

	// graph mapping
	private static void fillGraphColIdsCnts(String pGraphType, int pHlpIdx, int pColIdx, int pDataCnt, int pCatColId)
	{
		// add to graph mapping, if needed
		for(int i = 1; i < graphConfig[GRAPH_TYPE_STATS].length; i++)
		{
			// only for specific graph type
			if (((String)graphConfig[GRAPH_TYPE_STATS][i].get("CODE")).startsWith(pGraphType))
			{
				//System.out.println(Arrays.toString((String[])graphConfig[i].get("COLUMN_NAMES")));
				// vars
				String[] tmpn = (String[])graphConfig[GRAPH_TYPE_STATS][i].get("COLUMN_NAMES");
				int[] tmpi = (int[])graphConfig[GRAPH_TYPE_STATS][i].get("COLUMN_IDS");
				int found = 0;
				// all graph cols
				for(int j = 0; j < tmpn.length; j++)
				{
					if (tmpi[j] < 0 && helperCols[pHlpIdx][0].equals(tmpn[j]))
					{
						// we found the match, adjust idx
						found = 1;
						tmpi[j] = pColIdx;
						//System.out.println(helperCols[pHlpIdx][0] + ":" + tmpn[j]);
					}
				}
				// if we have adjustments
				if (found > 0)
				{
					graphConfig[GRAPH_TYPE_STATS][i].put("COLUMN_IDS", tmpi);
					graphConfig[GRAPH_TYPE_STATS][i].put("LAST_ROW", pDataCnt);
					graphConfig[GRAPH_TYPE_STATS][i].put("CAT_COL_ID", pCatColId);
					//System.out.println(Arrays.toString((int[])graphConfig[GRAPH_TYPE_STATS][i].get("COLUMN_IDS")));
				}
			}
		}
	}

	// this will set up maps for calculations sheet
	private static void prepareFinalCalcMaps(int pDataCnt)
	{
		// const
		final int colCntTot = 4; // this sets how many different stats will be computed (align with DYNAMIC cols code below)
		// mapping of additional cols (daya / session)
		HashMap<String, String[]> hlpMap = new HashMap<>();
		// purely calc cols
		int nullColCount = 0;
		for(int i = 0; i < helperCols.length; i++) nullColCount += (helperCols[i][2] == null) ? 1 : 0;
		// vars
		int colIdx, colCnt = -1;
		String pref, post, col;

		// final map
		calcMapDet = new HashMap[helperCols.length + (helperCols.length - nullColCount) * colCntTot];

		// HELPER cols
		for(int i = 0; i < helperCols.length; i++)
		{
			// col count
			colCnt++;
			// prepare column from which to calculate (SOURCE COL)
			pref = helperCols[i][0];
			calcMapDet[colCnt] = new HashMap();
			calcMapDet[colCnt].put("COL_HEAD_TEXT", pref);
			calcMapDet[colCnt].put("COL_HEAD_FORMULA", null);
			calcMapDet[colCnt].put("COL_HEAD_SYLE", "n");
			calcMapDet[colCnt].put("COL_VAL_FORMULA", helperCols[i][1]);
			calcMapDet[colCnt].put("COL_VAL_SYLE", helperCols[i][2]);
			calcMapDet[colCnt].put("COL_WIDTH", Math.max(pref.length(), 10) * 300);
			calcMapDet[colCnt].put("DATA_GRAPH_TYPE", null);
			calcMapDet[colCnt].put("DATA_FROM", null);
			calcMapDet[colCnt].put("DATA_TO", null);
			// additional helper colums
			if (helperCols[i][2] == null)
				hlpMap.put(helperCols[i][0], new String[]{String.valueOf(colCnt), CellReference.convertNumToColString(colCnt)});
		}

		// DYNAMIC cols
		for(int i = 0; i < helperCols.length; i++)
		{
			// skip ones we don't need
			if (helperCols[i][2] == null)
				continue;
			//System.out.println(colCnt + " | " + calcMapDet.length);
			// col count
			colCnt++;
			col = CellReference.convertNumToColString(i);
			// prepare column from which to calculate (AVG COL)
			pref = "AVG "; post = " %";
			calcMapDet[colCnt] = new HashMap();
			calcMapDet[colCnt].put("COL_HEAD_TEXT", pref + helperCols[i][0].toLowerCase() + post);
			calcMapDet[colCnt].put("COL_HEAD_FORMULA", null);
			calcMapDet[colCnt].put("COL_HEAD_SYLE", "b");
			calcMapDet[colCnt].put("COL_VAL_FORMULA", "ROUND(AVERAGE($" + col + "$2:$" + col + "_ROW_),2)");
			calcMapDet[colCnt].put("COL_VAL_SYLE", helperCols[i][2]);
			calcMapDet[colCnt].put("COL_WIDTH", (pref.length() + helperCols[i][0].length() + post.length()) * 300);
			calcMapDet[colCnt].put("SER_COLOR", helperCols[i][3]);
			// add to graph mapping, if needed
			fillGraphColIdsCnts("AVG_TOTAL", i, colCnt, pDataCnt, Integer.parseInt(hlpMap.get("Nr")[0]));

			// col count
			colCnt++;
			// prepare column from which to calculate (SLIDING n AVG COL)
			pref = "\"Rolling (\",constants!$B$1,\") AVG "; post = " %\"";
			calcMapDet[colCnt] = new HashMap();
			calcMapDet[colCnt].put("COL_HEAD_TEXT", null);
			calcMapDet[colCnt].put("COL_HEAD_FORMULA", "CONCATENATE(" + pref + helperCols[i][0].toLowerCase() + post + ")");
			calcMapDet[colCnt].put("COL_HEAD_SYLE", "b");
			calcMapDet[colCnt].put("COL_VAL_FORMULA", "ROUND(AVERAGE(OFFSET($" + col + "_ROW_,IF(COUNTA($" + col + "$2:$" + col + "_ROW_)>constants!$B$1,(constants!$B$1*(-1)+1),-COUNTA($" + col + "$2:$" + col + "_ROW_)+1),0):$" + col + "_ROW_),2)");
			calcMapDet[colCnt].put("COL_VAL_SYLE", helperCols[i][2]);
			calcMapDet[colCnt].put("COL_WIDTH", (pref.length() + helperCols[i][0].length()-16 + post.length()) * 300);
			calcMapDet[colCnt].put("SER_COLOR", helperCols[i][3]);
			// add to graph mapping, if needed
			fillGraphColIdsCnts("AVG_SLIDING_N", i, colCnt, pDataCnt, Integer.parseInt(hlpMap.get("Nr")[0]));

			// col count
			colCnt++;
			// prepare column from which to calculate (SLIDING DAY AVG COL)
			pref = "\"Rolling (\",constants!$B$2,\") AVG "; post = " %\"";
			calcMapDet[colCnt] = new HashMap();
			calcMapDet[colCnt].put("COL_HEAD_TEXT", null);
			calcMapDet[colCnt].put("COL_HEAD_FORMULA", "CONCATENATE(" + pref + helperCols[i][0].toLowerCase() + post + ")");
			calcMapDet[colCnt].put("COL_HEAD_SYLE", "b");
			calcMapDet[colCnt].put("COL_VAL_FORMULA", "ROUND(AVERAGE(OFFSET($" + col + "_ROW_,(-COUNTIFS($" + hlpMap.get("Day")[1] + "$2:$" + hlpMap.get("Day")[1] + "_ROW_," + hlpMap.get("Day")[1] + "_ROW_)+1),0):$" + col + "_ROW_),2)");
			calcMapDet[colCnt].put("COL_VAL_SYLE", helperCols[i][2]);
			calcMapDet[colCnt].put("COL_WIDTH", (pref.length() + helperCols[i][0].length()-16 + post.length()) * 300);
			calcMapDet[colCnt].put("SER_COLOR", helperCols[i][3]);
			// add to graph mapping, if needed
			fillGraphColIdsCnts("AVG_SLIDING_DAY", i, colCnt, pDataCnt, Integer.parseInt(hlpMap.get("Day")[0]));

			// col count
			colCnt++;
			// prepare column from which to calculate (SLIDING SESSION AVG COL)
			pref = "\"Rolling (\",constants!$B$3,\") AVG "; post = " %\"";
			calcMapDet[colCnt] = new HashMap();
			calcMapDet[colCnt].put("COL_HEAD_TEXT", null);
			calcMapDet[colCnt].put("COL_HEAD_FORMULA", "CONCATENATE(" + pref + helperCols[i][0].toLowerCase() + post + ")");
			calcMapDet[colCnt].put("COL_HEAD_SYLE", "b");
			calcMapDet[colCnt].put("COL_VAL_FORMULA", "ROUND(AVERAGE(OFFSET($" + col + "_ROW_,(-COUNTIFS($" + hlpMap.get("Session Nr.")[1] + "$2:$" + hlpMap.get("Session Nr.")[1] + "_ROW_," + hlpMap.get("Session Nr.")[1] + "_ROW_)+1),0):$" + col + "_ROW_),2)");
			calcMapDet[colCnt].put("COL_VAL_SYLE", helperCols[i][2]);
			calcMapDet[colCnt].put("COL_WIDTH", (pref.length() + helperCols[i][0].length()-16 + post.length()) * 300);
			calcMapDet[colCnt].put("SER_COLOR", helperCols[i][3]);
			// add to graph mapping, if needed
			fillGraphColIdsCnts("AVG_SLIDING_SESS", i, colCnt, pDataCnt, Integer.parseInt(hlpMap.get("Session Nr.")[0]));
		}

		/*
		for(int i = 1; i < graphConfig[GRAPH_TYPE_STATS].length; i++)
		{
			System.out.println(i + ": " + Arrays.toString((String[])graphConfig[GRAPH_TYPE_STATS][i].get("COLUMN_NAMES")) + ", " + Arrays.toString((int[])graphConfig[GRAPH_TYPE_STATS][i].get("COLUMN_IDS")));
		}
		*/
	}

	// this will read the file and parse it's contents
	private static ArrayList<String[]> readFile(String pFileName) throws Exception
	{
		// vars
		ArrayList<String[]> fileContents = new ArrayList<String[]>();
		String fline;
		String[] line;
		int lineCnt = 0;

		// get reader stream
		BufferedReader fileRead = new BufferedReader(new FileReader(pFileName));
		// read the first line (should be header row)
		fline = fileRead.readLine();
		// read the data
		while (fline != null)
		{
			// line cnt
			lineCnt++;
			// get columns
			line = fline.split(";");
			// allocate memory for columns
			fileContents.add(line);
			// read the first line (should be header row)
			fline = fileRead.readLine();
		}

		// results
		return(fileContents);
	}

	// process and save results
	private static void processAndSaveRsults(ArrayList<String[]> pFileContents, String outFile) throws Exception
	{
		// col positions
		HashMap[] colPositions;
		// output file name
		FileOutputStream wotfOut = new FileOutputStream(outFile);
		// create a workbook
		XSSFWorkbook wotwb = new XSSFWorkbook();
		// creat a bold style
		XSSFCellStyle wotBoldStyle = (XSSFCellStyle)wotwb.createCellStyle();
		Font wotFontBold = wotwb.createFont();
		wotFontBold.setFontName("Arial");
		wotFontBold.setFontHeightInPoints((short)10);
		wotFontBold.setBold(true);
		wotBoldStyle.setFont(wotFontBold);
		// creat a bold + italic style
		XSSFCellStyle wotItalicStyle = (XSSFCellStyle)wotwb.createCellStyle();
		Font wotFontItalic = wotwb.createFont();
		wotFontItalic.setFontName("Arial");
		wotFontItalic.setFontHeightInPoints((short)10);
		wotFontItalic.setItalic(true);
		wotItalicStyle.setFont(wotFontItalic);
		// create regular style
		XSSFCellStyle wotRegularStyle = (XSSFCellStyle)wotwb.createCellStyle();
		Font wotFont = wotwb.createFont();
		wotFont.setFontName("Arial");
		wotFont.setFontHeightInPoints((short)10);
		wotRegularStyle.setFont(wotFont);
		// create regular style with percent
		XSSFCellStyle wotRegularStylePercent = (XSSFCellStyle)wotwb.createCellStyle();
		Font wotFontPercent = wotwb.createFont();
		wotFontPercent.setFontName("Arial");
		wotFontPercent.setFontHeightInPoints((short)10);
		wotRegularStylePercent.setDataFormat(wotwb.createDataFormat().getFormat("0.00%"));
		wotRegularStylePercent.setFont(wotFontPercent);

		// stats sheet creation
		createStatsSheet(pFileContents, wotwb, wotRegularStyle, wotBoldStyle);

		// create constants sheet
		createConstantsSheet(wotwb, wotBoldStyle, wotItalicStyle);

		// create calc sheet
		createCalcSheet(wotwb, pFileContents.size(), wotRegularStyle, wotBoldStyle, wotRegularStylePercent);

		// create a graph sheet
		createGraphSheet(wotwb);

		// create a spread sheet
		createSpreadSheet(wotwb, pFileContents.size(), wotBoldStyle, wotRegularStylePercent);

		// evaluate all formulas
		wotwb.getCreationHelper().createFormulaEvaluator().evaluateAll();

		// write the file
		wotwb.write(wotfOut);
		wotfOut.close();
	}

	// create stats sheet
	private static void createStatsSheet(ArrayList<String[]> pFileContents, XSSFWorkbook pWotwb, XSSFCellStyle pStyleReg, XSSFCellStyle pStyleBold)
	{
		// vars
		int[] maxColSizes = new int[pFileContents.get(0).length];
		String[] wotline;
		XSSFRow wotrow;
		XSSFCell wotcell;

		// create a sheet
		XSSFSheet wotsheet = pWotwb.createSheet("stats");

		// looping through data
		for(int i = 0; i < pFileContents.size(); i++)
		{
			// vars
			wotrow = wotsheet.createRow(i);
			// now create as much cells as needed
			wotline = pFileContents.get(i);
			// columns
			for(int j = 0; j < wotline.length; j++)
			{
				// cell
				wotcell = wotrow.createCell(j);
				if (i == 0)
					wotcell.setCellStyle(pStyleBold);
				else
					wotcell.setCellStyle(pStyleReg);
				// value
				if (checkNumber(wotline[j]))
					wotcell.setCellValue(Double.parseDouble(wotline[j]));
				else
					wotcell.setCellValue(wotline[j]);
				// determine value size
				maxColSizes[j] = maxColSizes[j] < wotline[j].length() ? wotline[j].length() : maxColSizes[j];
			}
		}

		// set col widths (according to the data)
		for(int i = 0; i < maxColSizes.length; i++)
		{
			// default col len
			wotsheet.setColumnWidth(i, maxColSizes[i] * 300);
		}
	}

	// create stats sheet
	private static void createConstantsSheet(XSSFWorkbook pWotwb, XSSFCellStyle pStyleBold, XSSFCellStyle pStyleItalic)
	{
		// vars
		String[] wotline;
		XSSFRow wotrow;
		XSSFCell wotcell;
		// contents
		String[][] contents = {
			{"Sliding game count", "20", "specify game count to calculate sliding stats"},
			{"Day game count", "day", "do not touch this (just a constant)"},
			{"Session game count", "session", "do not touch this (just a constant)"}};
		int[] maxColSizes = new int[contents[0].length];

		// create a sheet
		XSSFSheet wotsheet = pWotwb.createSheet("constants");

		// looping through data
		for(int i = 0; i < contents.length; i++)
		{
			// vars
			wotrow = wotsheet.createRow(i);
			// now create as much cells as needed
			wotline = contents[i];
			// columns
			for(int j = 0; j < wotline.length; j++)
			{
				// cell
				wotcell = wotrow.createCell(j);
				// olny first 2 cols
				if (j < 2)
					wotcell.setCellStyle(pStyleBold);
				else
					wotcell.setCellStyle(pStyleItalic);
				// value
				if (checkNumber(wotline[j]))
					wotcell.setCellValue(Double.parseDouble(wotline[j]));
				else
					wotcell.setCellValue(wotline[j]);
				// determine value size
				maxColSizes[j] = maxColSizes[j] < wotline[j].length() ? wotline[j].length() : maxColSizes[j];
			}
		}

		// set col widths (according to the data)
		for(int i = 0; i < maxColSizes.length; i++)
		{
			// default col len
			wotsheet.setColumnWidth(i, maxColSizes[i] * 300);
		}
	}

	// create calc sheet
	private static void createCalcSheet(XSSFWorkbook pWotwb, int pRowCount, XSSFCellStyle pStyleReg, XSSFCellStyle pStyleBold, XSSFCellStyle pStylePercent)
	{
		// vars
		String colTxt;
		String colFrm;
		XSSFCellStyle colStyle;
		XSSFRow wotrow;
		XSSFCell wotcell;

		// create a sheet
		XSSFSheet wotsheet = pWotwb.createSheet("calc");

		// process rows
		for(int i = 0; i < pRowCount; i++)
		{
			// vars
			wotrow = wotsheet.createRow(i);

			// process cols
			for(int j = 0; j < calcMapDet.length; j++)
			{
				// cell
				wotcell = wotrow.createCell(j);
				// column text
				colTxt = null;
				colFrm = null;
				//System.out.println(j + ":" + (String)pColMap[j].get("COL_HEAD_TEXT"));
				// set up column header for first row
				if (i == 0)
				{
					// style
					colStyle = pStyleReg;
					// formula or text
					if (calcMapDet[j].get("COL_HEAD_TEXT") != null)
						colTxt = (String)calcMapDet[j].get("COL_HEAD_TEXT");
					else
						colFrm = (String)calcMapDet[j].get("COL_HEAD_FORMULA");
					// style
					if ("b".equals((String)calcMapDet[j].get("COL_HEAD_SYLE")))
						colStyle = pStyleBold;
					// set up column width
					wotsheet.setColumnWidth(j, (int)calcMapDet[j].get("COL_WIDTH"));
				}
				else
				{
					colStyle = "%".equals((String)calcMapDet[j].get("COL_VAL_SYLE")) ? pStylePercent : pStyleReg;
					// data formula
					colFrm = (String)calcMapDet[j].get("COL_VAL_FORMULA");
				}

				// set up cell things
				if (colTxt != null)
					wotcell.setCellValue(colTxt);
				else
				{
					// prepare and replace things in formula
					colFrm = colFrm.replaceAll("_ROW_", String.valueOf(i + 1));
					//System.out.println(colFrm);
					// set formula
					wotcell.setCellFormula(colFrm);
				}
				// style
				wotcell.setCellStyle(colStyle);
			}
		}
	}

	// create calc sheet
	private static void createGraphSheet(XSSFWorkbook pWotwb) throws Exception
	{
		// create a sheet
		XSSFSheet wotsheet = pWotwb.createSheet("graph");
		// no grid lines on graph page
		wotsheet.setDisplayGridlines(false);
		// zoom to 90, to see whole graph
		//wotsheet.setZoom(90);

		// create graphs
		createGraph(pWotwb, GRAPH_TYPE_STATS, "graph", "calc");
	}

	// create stats sheet
	private static void createSpreadSheet(XSSFWorkbook pWotwb, int pRowCount, XSSFCellStyle pStyleBold, XSSFCellStyle pStylePercent) throws Exception
	{
		// vars
		String[] wotline;
		XSSFRow wotrow;
		XSSFCell wotcell;
		int currRow = 0, currCell = 0, st = 0, en = 0;
		// contents
		String[][] contents = {
			{"Tier diff", "Battle count", "Tier spread %"},
			{"Battle len", "Battle count", "Battle len spread %"},
			{"Score diff", "Battle count", "Score diff spread %"}};
		int[] maxColSizes = new int[contents[0].length];

		// create a sheet
		XSSFSheet wotsheet = pWotwb.createSheet("spread");

		// config
		for(int currStat = 0; currStat < contents.length; currStat++)
		{
			// title
			wotrow = wotsheet.createRow(currRow++); currCell = 0;
			// header
			for(int i = 0; i < contents.length; i++)
			{
				// cell
				wotcell = wotrow.createCell(currCell++);
				// olny first 2 cols
				wotcell.setCellStyle(pStyleBold);
				// header text
				wotcell.setCellValue(contents[currStat][i]);
				// determine value size
				maxColSizes[i] = maxColSizes[i] < contents[currStat][i].length() ? contents[currStat][i].length() : maxColSizes[i];
			}

			// Tier spread
			if (currStat == 0)
			{
				st = -2; en = 2;
				for(int i = st; i <= en; i++)
				{
					// title
					wotrow = wotsheet.createRow(currRow++); currCell = 0;
					// header text
					wotcell = wotrow.createCell(currCell++); wotcell.setCellValue(i);
					// count cell
					wotcell = wotrow.createCell(currCell++);
					wotcell.setCellFormula("COUNTIFS(stats!$" + srcColLocs.get("Tier diff").get("COL") + "$2:stats!$" + srcColLocs.get("Tier diff").get("COL") + "$" + pRowCount + ",A" + currRow + ")");
					// % cell
					wotcell = wotrow.createCell(currCell++);
					wotcell.setCellStyle(pStylePercent);
					wotcell.setCellFormula("$B" + currRow + "/SUM($B$" + (currRow + st - i) + ":$B$" + (currRow + en - i) + ")");
				}
			}
			// Battle spread
			else if (currStat == 1)
			{
				st = 1; en = 15;
				for(int i = st; i <= en; i++)
				{
					// title
					wotrow = wotsheet.createRow(currRow++); currCell = 0;
					// header text
					wotcell = wotrow.createCell(currCell++); wotcell.setCellValue(i);
					// count cell
					wotcell = wotrow.createCell(currCell++);
					wotcell.setCellFormula("COUNTIFS(stats!$" + srcColLocs.get("Battle len").get("COL") + "$2:stats!$" + srcColLocs.get("Battle len").get("COL") + "$" + pRowCount + ",\"<=\"&A" + currRow + "*60"
						+ ",stats!$" + srcColLocs.get("Battle len").get("COL") + "$2:stats!$" + srcColLocs.get("Battle len").get("COL") + "$" + pRowCount + ",\">\"&(A" + currRow + "-1)*60)");
					// % cell
					wotcell = wotrow.createCell(currCell++);
					wotcell.setCellStyle(pStylePercent);
					wotcell.setCellFormula("$B" + currRow + "/SUM($B$" + (currRow + st - i) + ":$B$" + (currRow + en - i) + ")");
				}
			}
			// Score spread
			else if (currStat == 2)
			{
				st = 0; en = 15;
				for(int i = st; i <= en; i++)
				{
					// title
					wotrow = wotsheet.createRow(currRow++); currCell = 0;
					// header text
					wotcell = wotrow.createCell(currCell++); wotcell.setCellValue(i);
					// count cell
					wotcell = wotrow.createCell(currCell++);
					wotcell.setCellFormula("COUNTIFS(stats!$" + srcColLocs.get("Score diff").get("COL") + "$2:stats!$" + srcColLocs.get("Score diff").get("COL") + "$" + pRowCount + ",A" + currRow + ")"
						+ "+IF(A" + currRow + "=0,0,COUNTIFS(stats!$" + srcColLocs.get("Score diff").get("COL") + "$2:stats!$" + srcColLocs.get("Score diff").get("COL") + "$" + pRowCount + ",-A" + currRow + "))");
					// % cell
					wotcell = wotrow.createCell(currCell++);
					wotcell.setCellStyle(pStylePercent);
					wotcell.setCellFormula("$B" + currRow + "/SUM($B$" + (currRow + st - i) + ":$B$" + (currRow + en - i) + ")");

				}
			}
			// empty row
			wotrow = wotsheet.createRow(currRow++);
		}

		// set col widths (according to the data)
		for(int i = 0; i < maxColSizes.length; i++)
		{
			// default col len
			wotsheet.setColumnWidth(i, maxColSizes[i] * 300);
		}

		// create some graphs
		createGraph(pWotwb, GRAPH_TYPE_SPREAD, "spread", "spread");
	}

	// create calc sheet
	private static void createGraph(XSSFWorkbook pWotwb, int pGraphType, String pGraphSheet, String pCalcSheet) throws Exception
	{
		// get graph sheet
		XSSFSheet wotsheet = pWotwb.getSheet(pGraphSheet);
		// get calc sheet
		XSSFSheet wotcalcsheet = pWotwb.getSheet(pCalcSheet);

		// lets create drawings for actual graphs
		for(int i = GRAPH_COMMON + 1; i < graphConfig[pGraphType].length; i++)
		{
			// null check
			if (graphConfig[pGraphType][i] == null)
				break;
			//System.out.println(Arrays.toString((String[])graphConfig[pGraphType][i].get("COLUMN_NAMES")) + " : " + Arrays.toString((int[])graphConfig[pGraphType][i].get("COLUMN_IDS")));
			// create drawing
			XSSFDrawing wotdrawing = wotsheet.createDrawingPatriarch();
			// create anchor
			XSSFClientAnchor wotanchor = (XSSFClientAnchor)wotdrawing.createAnchor(0, 0, 0, 0, (int)graphConfig[pGraphType][GRAPH_COMMON].get("GRAPH_START_COL"), (i-1)*(int)graphConfig[pGraphType][GRAPH_COMMON].get("GRAPH_HEIGHT"), (int)graphConfig[pGraphType][GRAPH_COMMON].get("GRAPH_WIDTH"), i*(int)graphConfig[pGraphType][GRAPH_COMMON].get("GRAPH_HEIGHT"));

			// create a chart
			XSSFChart wotchart = wotdrawing.createChart((ClientAnchor)wotanchor);
			wotchart.setTitleText((String)graphConfig[pGraphType][i].get("TITLE"));
			wotchart.setTitleOverlay(false);

			// create legends
			XDDFChartLegend wotlegend = wotchart.getOrAddLegend();
			wotlegend.setPosition(LegendPosition.RIGHT);

			XDDFCategoryAxis wotbottomaxis = wotchart.createCategoryAxis(AxisPosition.BOTTOM);
			wotbottomaxis.setTitle((String)graphConfig[pGraphType][i].get("CAT_TITLE"));
			wotbottomaxis.setCrosses(AxisCrosses.AUTO_ZERO);
			XDDFValueAxis wotleftaxis = wotchart.createValueAxis(AxisPosition.RIGHT);
			wotleftaxis.setCrosses(AxisCrosses.AUTO_ZERO);
			wotleftaxis.setTitle((String)graphConfig[pGraphType][i].get("VAL_TITLE"));

			// create data
			XDDFLineChartData wotdata = (XDDFLineChartData)wotchart.createData(ChartTypes.LINE, wotbottomaxis, wotleftaxis);

			// get series
			int[] seriesColIds = (int[])graphConfig[pGraphType][i].get("COLUMN_IDS");
			int catColId = (int)graphConfig[pGraphType][i].get("CAT_COL_ID");
			//String[] seriesColNames = (String[])graphConfig[pGraphType][i].get("COLUMN_NAMES"); // for debug

			// go through all series
			for(int j = 0; j < seriesColIds.length; j++)
			{
				// color
				String color = graphConfig[pGraphType][i].containsKey("SER_COLOR") ? (String)graphConfig[pGraphType][i].get("SER_COLOR") : (String)calcMapDet[seriesColIds[j]].get("SER_COLOR");
				// create datasources
				XDDFDataSource<String> wotgames = XDDFDataSourcesFactory.fromStringCellRange(wotcalcsheet, new CellRangeAddress((int)graphConfig[pGraphType][i].get("FIRST_ROW"), (int)graphConfig[pGraphType][i].get("LAST_ROW"), catColId, catColId));
				XDDFNumericalDataSource<Double> wotstats = XDDFDataSourcesFactory.fromNumericCellRange(wotcalcsheet, new CellRangeAddress((int)graphConfig[pGraphType][i].get("FIRST_ROW"), (int)graphConfig[pGraphType][i].get("LAST_ROW"), seriesColIds[j], seriesColIds[j]));
				// add series
				XDDFLineChartData.Series wotseries = (XDDFLineChartData.Series)wotdata.addSeries(wotgames, wotstats);
				wotseries.setSmooth(true);
				wotseries.setMarkerStyle(MarkerStyle.NONE);
				//System.out.println(seriesColNames[j] + ":" + seriesColIds[j]);
				wotseries.setTitle(null, new CellReference((String)graphConfig[pGraphType][GRAPH_COMMON].get("CALC_SHEET") + "!$" + CellReference.convertNumToColString(seriesColIds[j]) + "$" + (int)graphConfig[pGraphType][i].get("FIRST_ROW")));
				setLineSeriesColor(wotseries, XDDFColor.from(hex2Rgb(color)));
			}

			// add text properties
			wotbottomaxis.getOrAddTextProperties();
			// rotation
			java.lang.reflect.Field _ctCatAx = XDDFCategoryAxis.class.getDeclaredField("ctCatAx");
			_ctCatAx.setAccessible(true);
			org.openxmlformats.schemas.drawingml.x2006.chart.CTCatAx ctCatAx = (org.openxmlformats.schemas.drawingml.x2006.chart.CTCatAx)_ctCatAx.get(wotbottomaxis);
			org.openxmlformats.schemas.drawingml.x2006.main.CTTextBody text = ctCatAx.getTxPr();
			text.getBodyPr().setRot((int)graphConfig[pGraphType][GRAPH_COMMON].get("LABEL_ROT"));

			// title deletion?
			if (wotchart.getCTChart().getAutoTitleDeleted() == null) wotchart.getCTChart().addNewAutoTitleDeleted();
			wotchart.getCTChart().getAutoTitleDeleted().setVal(false);
			// white fill
			wotchart.getCTChartSpace().addNewSpPr().addNewSolidFill().addNewSrgbClr().setVal(hex2Rgb((String)graphConfig[pGraphType][GRAPH_COMMON].get("FILL_COLOR")));
			// middle axis
			wotchart.getCTChart().getPlotArea().getCatAxArray(0).addNewSpPr().addNewLn().addNewSolidFill().addNewSrgbClr().setVal(hex2Rgb((String)graphConfig[pGraphType][GRAPH_COMMON].get("GRID_COLOR")));
			// value axis
			wotchart.getCTChart().getPlotArea().getValAxArray(0).addNewSpPr().addNewLn().addNewSolidFill().addNewSrgbClr().setVal(hex2Rgb((String)graphConfig[pGraphType][GRAPH_COMMON].get("GRID_COLOR")));
			// grid lines
			wotchart.getCTChart().getPlotArea().getValAxArray()[0].addNewMajorGridlines().addNewSpPr().addNewLn().addNewSolidFill().addNewSrgbClr().setVal(hex2Rgb((String)graphConfig[pGraphType][GRAPH_COMMON].get("GRID_COLOR")));

			// % step
			wotleftaxis.setMajorUnit(0.1);

			// fill the data
			wotchart.plot(wotdata);
		}
	}

	// set line color
	private static void setLineSeriesColor(XDDFChartData.Series series, XDDFColor color)
	{
		XDDFSolidFillProperties fill = new XDDFSolidFillProperties(color);
		XDDFLineProperties line = new XDDFLineProperties();
		line.setWidth((Double)1.55);
		line.setFillProperties(fill);
		XDDFShapeProperties properties = series.getShapeProperties() != null ? series.getShapeProperties() : new XDDFShapeProperties();
		properties.setLineProperties(line);
		series.setShapeProperties(properties);
	}

	// convert color hex representation to byte array
	private static byte[] hex2Rgb(String colorStr)
	{
		int r = Integer.valueOf(colorStr.substring(1, 3), 16);
		int g = Integer.valueOf(colorStr.substring(3, 5), 16);
		int b = Integer.valueOf(colorStr.substring(5, 7), 16);
		return new byte[]{(byte)r, (byte)g, (byte)b};
	}

	// helper to get float
	private static boolean checkNumber(String pValue)
	{
		// try parsing
		try
		{
			Double.parseDouble(pValue);
			return(true);
		}
		catch (NumberFormatException nfe)
		{
			return(false);
		}
	}
}
