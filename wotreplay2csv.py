#!/usr/bin/env python
# -*- coding: utf-8 -*-

#################################
#   WOT replay parser to CSV    #
#         by MjasnikWOT         #
#  if you use this in any way   #
#   please leave this note in,  #
#           thanks              #
#################################

# imports
import os, glob, struct, json, time, sys, datetime, re

# constants
# arguments
_FCNT = "FCNT"  # battle file count
_LOGL = "LOGLEVEL"  # battle file count (0 - normal, 1 - debug)
_WARNSTOP = "WARNSTOP"  # whether to stop on important warnings
_FREPDIR = "REPDIR"  # replay directory
_FTNKDIR = "TNKDIR"  # tanks description directory
_FRESDIR = "RESDIR"  # results file directory
_FLOCDIR = "LOCDIR"  # locations file directory
_IDLESECS = "IDLESECS"  # how long session is still considered active
_TANKF = "TANKF"  # whether to generate separate CSV for each tank
_REPSPLIT = "REPSPLIT"  # split resulting file in week or month (disables individual tank file generation)
_ABSMMSPR = "ABSMMSPR"  # this was meant as argument, but not yet
# data blocks
_NBLK = "NBLK"  # number of blocks
_GAMEBLK = "GAMEBLK"  # main game result block (version, players, ids, etc.)
_RESBLK = "RESBLK"  # game results block (gamage, xp, team, etc.)
_PLBLK = "PLAYERBLK"  # player blocks (which tank, etc.)
_FRAGBLK = "FRAGBLK"  # frags blk
_PLINFO = "PLINFO"  # player information
_BATRES = "BATRES"  # final results information
# helpers
_TNKBYNAME = "TANKSBYNAME"  # special tag to find tanks by name
# variables
# arguments
ARGS = dict()
# countries data
COUNTRIES = dict()
# tanks data
TANKS = dict()
# set up replay data variable
DATA = dict()
# data structure to help with parsing files at once
DATAX = dict()

# defaults
ARGS[_FTNKDIR] = "tanks"
ARGS[_FREPDIR] = "replays"
ARGS[_FRESDIR] = "results"
ARGS[_FLOCDIR] = "countries"
ARGS[_FCNT] = 999999999999
ARGS[_LOGL] = 1
ARGS[_WARNSTOP] = 1
ARGS[_IDLESECS] = 3600
ARGS[_TANKF] = 0
ARGS[_REPSPLIT] = None
ARGS[_ABSMMSPR] = 2

def main():
    # global modifiers
    global DATA
    # vars
    battleCnt = 0
    battleCntComp = 0

    # ### start reading files ###
    for RFILE in sorted(glob.glob("%s/%s" % (ARGS[_FREPDIR], "*.wotreplay"))):
        # vars
        isFileOk = True
        battleCnt += 1

        # open file and try to read it
        with open(RFILE, 'rb') as f:
            # set up replay data variable
            DATA = dict()
            # defaults
            DATA[_NBLK] = 0
            DATA[_PLINFO] = dict()
            DATA[_GAMEBLK] = dict()
            DATA[_RESBLK] = dict()
            DATA[_PLBLK] = dict()
            DATA[_FRAGBLK] = dict()
            DATA[_BATRES] = dict()

            try:
                # dgb
                log(0, "---\nProcessing file: %s" % (RFILE))
                # check number of blocks
                f.seek(4)
                # save number of blocks
                DATA[_NBLK] = struct.unpack("I",f.read(4))[0]
                # dbg
                log(2, "Found blocks: %i" % (DATA[_NBLK]))

                # vars
                startPointer = 8
                # processing data for blocks
                for RBLK in range(0, DATA[_NBLK]):
                    log(2, "  Retrieving data for block: %i" % (RBLK + 1))
                    # seek to start of the file
                    f.seek(startPointer)
                    # get the size of the block
                    bsize = struct.unpack("I", f.read(4))[0]
                    # advance start
                    startPointer += 4
                    # advance to start of data
                    f.seek(startPointer)
                    # calc start of next block
                    startPointer += bsize
                    # read the contents
                    fblk = f.read(bsize)

                    # ## HERE WE PARSE JSON ##
                    fjson = json.loads(fblk)

                    # # check what we have got
                    # base game is dict
                    if type(fjson) is dict and "clientVersionFromExe" in fjson and "playerName" in fjson:
                        log(2, "    Found main game block")
                        DATA[_GAMEBLK] = fjson
                    # second block with results
                    elif type(fjson) is list:
                        # dbg
                        log(2, "    Game results block, results lenght: %i" % (len(fjson)))
                        # try to determine which is the right block
                        for RBLKIDX in range(0, len(fjson)):
                            # check patterns
                            if "arenaUniqueID" in fjson[RBLKIDX] and "vehicles" in fjson[RBLKIDX]:
                                # dbg
                                log(2, "      Found main game results block")
                                # save main block
                                DATA[_RESBLK] = fjson[RBLKIDX]
                            else:
                                # try traversing the data block to see whether we have proper player block
                                for RPL in fjson[RBLKIDX]:
                                    # check for data
                                    if "team" in fjson[RBLKIDX][RPL] and "vehicleType" in fjson[RBLKIDX][RPL]:
                                        # dbg
                                        log(2, "      Found main game players block")
                                        # save data block
                                        DATA[_PLBLK] = fjson[RBLKIDX]
                                        # finish
                                        break
                                    elif "frags" in fjson[RBLKIDX][RPL]:
                                        # dbg
                                        log(2, "      Found main game frags block")
                                        # save data block
                                        DATA[_FRAGBLK] = fjson[RBLKIDX]
                                        # finish
                                        break
            except Exception as ex:
                # print error
                log(-99, str(ex), True)
                isFileOk = False

        # dbg
        if not isFileOk:
            log(-99, "WARN: file corrupted or not supported\n---")
            continue
        else:
            log(2, "File parsed sucessfully")

            # normalize game
            normalizeGameData()
            # print file info
            printFileInfo()

            # check if we have enough info to compute stats
            if not DATA[_GAMEBLK]:
                log(0, "WARN: file does not contain MAIN game block to compute stats, ignoring...")
                continue
            elif not DATA[_RESBLK]:
                log(0, "WARN: file does not contain BATTLE RESULTS game block to compute stats, ignoring...")
                continue
            elif not DATA[_PLBLK]:
                log(0, "WARN: file does not contain PLAYER game block to compute stats, ignoring...")
                continue

        # identify player (id, team)
        identifyPlayer()
        # load tank data according to game replay version
        loadTankData()
        # verify tanks, so we know they exist, check warning afterwards
        if not verifyTankData():
            log(0, "WARN: UNKNOWN tank in replay file, ignoring...")
            continue

        # pre compute needed values for all replays
        adjustSessionStats()
        # get player performance in battle
        getPlayerGameStatistics()
        # compute team performance in the game
        computeTeamStatistics()
        # compute stat difference %
        calculateStatPercentages()

        # save final results to files
        saveBattleResults()

        # count OK files
        battleCntComp += 1

        # TMP BREAK
        if battleCnt >= ARGS[_FCNT]:
            # stop
            break

    # log
    if battleCnt > 0:
        # log
        log(0, "---")

    # summary
    log(0, "\n--- Summary ---")
    log(0, "Replay count: %i" % (battleCnt))
    log(0, "Successfully processed: %i" % (battleCntComp))
    log(0, "Can not process: %i" % (battleCnt - battleCntComp))
    log(0, "---------------\n")

# ## helper functions ##

def log(pLvl, pText):
    # log
    if pLvl <= ARGS[_LOGL]:
        print(pText)
        if ARGS[_WARNSTOP] > 0 and pLvl > 0:
            exit(2)

def normalizeGameData():
    # vars
    global DATA
    # normalize version
    DATA[_GAMEBLK]["clientVersionFromExe"] = DATA[_GAMEBLK]["clientVersionFromExe"].replace(" ", "").replace(",", ".")
    # sometimes version is borked (0.0.0.0), let's try to find version otherwise
    if DATA[_GAMEBLK]["clientVersionFromExe"] == "0.0.0.0":
        log(0, "WARN: game version from exe is borked, will try version from xml")
        # tmp version
        versionTmp = re.sub(r"^World.*Tanks v.(.*) #[0-9]*", r"\1", DATA[_GAMEBLK]["clientVersionFromXml"])
        # check if we found the version
        if versionTmp != DATA[_GAMEBLK]["clientVersionFromXml"]:
            DATA[_GAMEBLK]["version"] = versionTmp
    else:
        DATA[_GAMEBLK]["version"] = DATA[_GAMEBLK]["clientVersionFromExe"]
    DATA[_GAMEBLK]["mapName"] = DATA[_GAMEBLK]["mapDisplayName"]
    # we need only first 3 numbers from name version
    while DATA[_GAMEBLK]["version"].count(".") > 2:
        # strip down version
        DATA[_GAMEBLK]["version"] = DATA[_GAMEBLK]["version"][0:DATA[_GAMEBLK]["version"].rfind(".")]

def printFileInfo():
    # print game info
    log(0, "Game version: %s (%s)" % (DATA[_GAMEBLK]["version"], DATA[_GAMEBLK]["clientVersionFromExe"]))
    log(2, "Map name: %s\nPlayerName: %s" % (DATA[_GAMEBLK]["mapDisplayName"], DATA[_GAMEBLK]["playerName"]))

def identifyPlayer():
    # global modifiers
    global DATA

    # try to determine player id, team, etc.
    for RPL in DATA[_PLBLK]:
        # find ourselves
        if DATA[_GAMEBLK]["playerName"] == DATA[_PLBLK][RPL]["name"]:
            # save team and id
            DATA[_PLINFO]["id"] = RPL
            DATA[_PLINFO]["name"] = DATA[_PLBLK][RPL]["name"]
            DATA[_PLINFO]["team"] = DATA[_PLBLK][RPL]["team"]
            DATA[_PLINFO]["isAlive"] = DATA[_PLBLK][RPL]["isAlive"]
            # dbg
            log(2, "Player identified in results structures")
            # player found
            break

    # find the tank player used
    DATA[_PLINFO]["tankCompDescr"] = DATA[_RESBLK]["vehicles"][DATA[_PLINFO]["id"]][0]["typeCompDescr"]

def loadCountryData():
    # global modifiers
    global COUNTRIES

    # load countries
    locfname = "countries.json"
    # we need to load countries file
    if not os.path.isfile(locfname):
        # we have a problem
        log(-99, "\nERROR: countries file %s not found" % (locfname))
        exit(2)
    else:
        # load
        with open(RFILE, 'r') as f:
            # assign to dict
            COUNTRIES = json.loads(f.read())

def loadTankData():
    # global modifiers
    global TANKS

    # find tanks file and load it
    if not DATA[_GAMEBLK]["version"] in TANKS:
        # tank file name
        tnkfname = "%s/tanks.%s.json" % (ARGS[_FTNKDIR], DATA[_GAMEBLK]["version"])
        # we need to load tanks file
        if not os.path.isfile(tnkfname):
            # we have a problem
            log(-99, "\nERROR: tanks file %s not found" % (tnkfname))
            exit(2)
        else:
            # load
            with open(tnkfname, 'r') as f:
                # assign to dict
                tnkfjson = json.loads(f.read())
                # now parse the file and load up the tanks for this version
                TANKS[DATA[_GAMEBLK]["version"]] = dict()
                TANKS[DATA[_GAMEBLK]["version"]][_TNKBYNAME] = dict()  # special meaning to find tanks by name
                # load in tanks
                for RTNK in tnkfjson:
                    # check for duplicates
                    if RTNK["compDescr"] in TANKS[DATA[_GAMEBLK]["version"]]:
                        log(0, "WARN: tank duplicates (compDescr: %s), check your tank file" % (RTNK["compDescr"]))
                    else:
                        # load up tanks (identified by compdescr)
                        TANKS[DATA[_GAMEBLK]["version"]][RTNK["compDescr"]] = {"name": RTNK["title"], "tier": RTNK["tier"], "premium": RTNK["premium"], "country": RTNK["countryid"], "type": RTNK["type_name"], "icon": RTNK["icon"].upper()}
                        # normalized tank name
                        TANKS[DATA[_GAMEBLK]["version"]][RTNK["compDescr"]]["nameNorm"] = TANKS[DATA[_GAMEBLK]["version"]][RTNK["compDescr"]]["name"].replace("/", "_").replace(" ", "_").replace("*", "_")

def verifyTankData():
    # vars
    result = True
    # check if all tanks exist
    for RPLID in DATA[_RESBLK]["vehicles"]:
        # vars
        tnk = DATA[_RESBLK]["vehicles"][RPLID][0]["typeCompDescr"]
        # check
        if tnk not in TANKS[DATA[_GAMEBLK]["version"]]:
            # no tank, generate error
            log(1, "ERROR: tank id: %i, name: %s does not exist in tanks file" % (tnk, DATA[_PLBLK][RPLID]["vehicleType"]))
            # fail
            result = False
    # result
    return(result)

def getPlayerGameStatistics():
    # global modifiers
    global DATA, DATAX

    # get game properties
    DATA[_BATRES]["gdate"] = str(DATAX["gdate"])  # these come from intermediate storage
    DATA[_BATRES]["gsessionnumber"] = str(DATAX["gsessionnumber"])  # these come from intermediate storage
    DATA[_BATRES]["mapName"] = DATA[_GAMEBLK]["mapName"]
    DATA[_BATRES]["gwinteam"] = DATA[_RESBLK]["common"]["winnerTeam"]
    DATA[_BATRES]["gbattlelen"] = DATA[_RESBLK]["common"]["duration"]
    DATA[_BATRES]["gwon"] = 1 if DATA[_BATRES]["gwinteam"] == DATA[_PLINFO]["team"] else 0
    # get player properties
    DATA[_BATRES]["gtank"] = TANKS[DATA[_GAMEBLK]["version"]][DATA[_PLINFO]["tankCompDescr"]]["name"]
    DATA[_BATRES]["gtanktype"] = TANKS[DATA[_GAMEBLK]["version"]][DATA[_PLINFO]["tankCompDescr"]]["type"]
    DATA[_BATRES]["gteam"] = DATA[_PLINFO]["team"]
    DATA[_BATRES]["gtier"] = TANKS[DATA[_GAMEBLK]["version"]][DATA[_PLINFO]["tankCompDescr"]]["tier"]
    # get player statistics
    DATA[_BATRES]["gmileage"] = DATA[_RESBLK]["vehicles"][DATA[_PLINFO]["id"]][0]["mileage"]
    DATA[_BATRES]["gtanksdamaged"] = DATA[_RESBLK]["vehicles"][DATA[_PLINFO]["id"]][0]["damaged"]
    DATA[_BATRES]["gxp"] = DATA[_RESBLK]["vehicles"][DATA[_PLINFO]["id"]][0]["xp"]
    DATA[_BATRES]["gkills"] = DATA[_RESBLK]["vehicles"][DATA[_PLINFO]["id"]][0]["kills"]
    DATA[_BATRES]["gdeath"] = -DATA[_RESBLK]["vehicles"][DATA[_PLINFO]["id"]][0]["deathCount"] # 0 - alive, -1 - dead
    DATA[_BATRES]["gspots"] = DATA[_RESBLK]["vehicles"][DATA[_PLINFO]["id"]][0]["spotted"]
    DATA[_BATRES]["glifetime"] = DATA[_RESBLK]["vehicles"][DATA[_PLINFO]["id"]][0]["lifeTime"]
    DATA[_BATRES]["gblocked"] = DATA[_RESBLK]["vehicles"][DATA[_PLINFO]["id"]][0]["damageBlockedByArmor"]
    DATA[_BATRES]["gshots"] = DATA[_RESBLK]["vehicles"][DATA[_PLINFO]["id"]][0]["shots"]
    DATA[_BATRES]["gpens"] = DATA[_RESBLK]["vehicles"][DATA[_PLINFO]["id"]][0]["piercings"]
    DATA[_BATRES]["ghits"] = DATA[_RESBLK]["vehicles"][DATA[_PLINFO]["id"]][0]["directHits"]
    DATA[_BATRES]["ghehits"] = DATA[_RESBLK]["vehicles"][DATA[_PLINFO]["id"]][0]["explosionHits"]
    DATA[_BATRES]["gtotalhits"] = DATA[_BATRES]["ghits"] + DATA[_BATRES]["ghehits"]
    DATA[_BATRES]["ghitsrec"] = DATA[_RESBLK]["vehicles"][DATA[_PLINFO]["id"]][0]["directHitsReceived"]
    DATA[_BATRES]["ghehitsrec"] = DATA[_RESBLK]["vehicles"][DATA[_PLINFO]["id"]][0]["explosionHitsReceived"]
    DATA[_BATRES]["gricochetsrec"] = DATA[_RESBLK]["vehicles"][DATA[_PLINFO]["id"]][0]["noDamageDirectHitsReceived"]
    DATA[_BATRES]["gdmg"] = DATA[_RESBLK]["vehicles"][DATA[_PLINFO]["id"]][0]["damageDealt"]
    DATA[_BATRES]["gassistdmg"] = 0
    for RDMG in ("damageAssistedStun", "damageAssistedTrack", "damageAssistedRadio"):
        if RDMG in DATA[_RESBLK]["vehicles"][DATA[_PLINFO]["id"]][0]:
            DATA[_BATRES]["gassistdmg"] += DATA[_RESBLK]["vehicles"][DATA[_PLINFO]["id"]][0][RDMG]
    DATA[_BATRES]["gtotaldmg"] = DATA[_BATRES]["gdmg"] + DATA[_BATRES]["gassistdmg"]
    DATA[_BATRES]["gdmgrec"] = DATA[_RESBLK]["vehicles"][DATA[_PLINFO]["id"]][0]["damageReceived"]
    DATA[_BATRES]["gpensrec"] = DATA[_RESBLK]["vehicles"][DATA[_PLINFO]["id"]][0]["piercingsReceived"]

def computeTeamStatistics():
    # global modifiers
    global DATA

    # initial team stats
    # XP
    DATA[_BATRES]["gtmaxxp"] = 0
    DATA[_BATRES]["gtminxp"] = 999999999999
    DATA[_BATRES]["gtavgxp"] = 0
    # dmg dealt, received
    DATA[_BATRES]["gtmaxdmg"] = 0
    DATA[_BATRES]["gtmindmg"] = 999999999999
    DATA[_BATRES]["gtavgdmg"] = 0
    # battle
    DATA[_BATRES]["gtplcnt"] = 0
    DATA[_BATRES]["gtscorediff"] = 0.0
    # tiers
    DATA[_BATRES]["gttopt"] = 0
    DATA[_BATRES]["gtbott"] = 99

    ### oppising team stats
    # xp
    DATA[_BATRES]["gtourxp"] = 0
    DATA[_BATRES]["gttheirxp"] = 0
    # damage
    DATA[_BATRES]["gtourdmg"] = 0
    DATA[_BATRES]["gtourassistdmg"] = 0
    DATA[_BATRES]["gttheirdmg"] = 0
    DATA[_BATRES]["gttheirassistdmg"] = 0
    DATA[_BATRES]["gtourdmgrec"] = 0
    DATA[_BATRES]["gttheirdmgrec"] = 0
    DATA[_BATRES]["gtourtotaldmg"] = 0
    DATA[_BATRES]["gttheirtotaldmg"] = 0
    # lifetime and survivors
    DATA[_BATRES]["gtouralive"] = 0
    DATA[_BATRES]["gttheiralive"] = 0
    DATA[_BATRES]["gtourlifetime"] = 0
    DATA[_BATRES]["gttheirlifetime"] = 0
    DATA[_BATRES]["gtouravglifetime"] = 0.0
    DATA[_BATRES]["gttheiravglifetime"] = 0.0
    # premium
    DATA[_BATRES]["gtourprems"] = 0
    DATA[_BATRES]["gttheirprems"] = 0
    DATA[_BATRES]["gtourpremdmg"] = 0.0
    DATA[_BATRES]["gttheirpremdmg"] = 0.0
    DATA[_BATRES]["gtpremtankdiff"] = 0
    # hits, shots, pens
    DATA[_BATRES]["gtourshots"] = 0
    DATA[_BATRES]["gtourhits"] = 0
    DATA[_BATRES]["gtourhehits"] = 0
    DATA[_BATRES]["gtourhitsrec"] = 0
    DATA[_BATRES]["gtourhehitsrec"] = 0
    DATA[_BATRES]["gtourpens"] = 0
    DATA[_BATRES]["gtourpensrec"] = 0
    DATA[_BATRES]["gttheirshots"] = 0
    DATA[_BATRES]["gttheirhits"] = 0
    DATA[_BATRES]["gttheirhehits"] = 0
    DATA[_BATRES]["gttheirhitsrec"] = 0
    DATA[_BATRES]["gttheirhehitsrec"] = 0
    DATA[_BATRES]["gttheirpens"] = 0
    DATA[_BATRES]["gttheirpensrec"] = 0
    DATA[_BATRES]["gtourtotalhits"] = 0
    DATA[_BATRES]["gttheirtotalhits"] = 0
    # tank damaged
    DATA[_BATRES]["gtourtanksdamaged"] = 0
    DATA[_BATRES]["gttheirtanksdamaged"] = 0
    # mileage
    DATA[_BATRES]["gtourmileage"] = 0
    DATA[_BATRES]["gttheirmileage"] = 0
    # ricochets
    DATA[_BATRES]["gttheirrico"] = 0
    DATA[_BATRES]["gtourrico"] = 0

    # compute battle stats for the team
    for RPLID in DATA[_RESBLK]["vehicles"]:
        # team id
        teamId = "our" if DATA[_RESBLK]["vehicles"][RPLID][0]["team"] == DATA[_PLINFO]["team"] else "their"
        # find tank player uses
        tnk = DATA[_RESBLK]["vehicles"][RPLID][0]["typeCompDescr"]
        # assist dmg
        gtAssDmg = 0
        # total DMG calc
        for RDMGT in ("damageAssistedStun", "damageAssistedTrack", "damageAssistedRadio"):
            # certain versions do not have all the data, we count what we have
            if RDMGT in DATA[_RESBLK]["vehicles"][RPLID][0]:
                # sum assist dmg
                gtAssDmg += DATA[_RESBLK]["vehicles"][RPLID][0][RDMGT]
        # count up players (does not matter in which team this is counted) and tiers
        DATA[_BATRES]["gtplcnt"] += (1 if teamId == "our" else 0)
        DATA[_BATRES]["gttopt"] = max(DATA[_BATRES]["gttopt"], TANKS[DATA[_GAMEBLK]["version"]][tnk]["tier"])
        DATA[_BATRES]["gtbott"] = min(DATA[_BATRES]["gtbott"], TANKS[DATA[_GAMEBLK]["version"]][tnk]["tier"])

        # we only care for our team for certain stats
        if teamId == "our":
            # XP calc (to compare to my damage)
            DATA[_BATRES]["gtmaxxp"] = max(DATA[_BATRES]["gtmaxxp"], DATA[_RESBLK]["vehicles"][RPLID][0]["xp"])
            DATA[_BATRES]["gtminxp"] = min(DATA[_BATRES]["gtminxp"], DATA[_RESBLK]["vehicles"][RPLID][0]["xp"])
            DATA[_BATRES]["gtavgxp"] += int(DATA[_RESBLK]["vehicles"][RPLID][0]["xp"])  # requires postprocessing
            # dmg calc (to comapare to my damage)
            DATA[_BATRES]["gtmaxdmg"] = max(DATA[_BATRES]["gtmaxdmg"], DATA[_RESBLK]["vehicles"][RPLID][0]["damageDealt"])
            DATA[_BATRES]["gtmindmg"] = min(DATA[_BATRES]["gtmindmg"], DATA[_RESBLK]["vehicles"][RPLID][0]["damageDealt"])
            DATA[_BATRES]["gtavgdmg"] += (DATA[_RESBLK]["vehicles"][RPLID][0]["damageDealt"] + gtAssDmg) # requires postprocessing

        ### opposing team stats
        # xp
        DATA[_BATRES]["gt%sxp" % (teamId)] += DATA[_RESBLK]["vehicles"][RPLID][0]["xp"]
        # dmg
        DATA[_BATRES]["gt%sdmg" % (teamId)] += DATA[_RESBLK]["vehicles"][RPLID][0]["damageDealt"]
        DATA[_BATRES]["gt%sassistdmg" % (teamId)] += gtAssDmg
        DATA[_BATRES]["gt%stotaldmg" % (teamId)] += DATA[_RESBLK]["vehicles"][RPLID][0]["damageDealt"] + gtAssDmg
        DATA[_BATRES]["gt%sdmgrec" % (teamId)] += DATA[_RESBLK]["vehicles"][RPLID][0]["damageReceived"]
        # lifetime and survivors
        DATA[_BATRES]["gt%salive" % (teamId)] -= DATA[_RESBLK]["vehicles"][RPLID][0]["deathCount"]  # requires postprocessing
        DATA[_BATRES]["gt%slifetime" % (teamId)]  += DATA[_RESBLK]["vehicles"][RPLID][0]["lifeTime"]
        # prem
        DATA[_BATRES]["gt%sprems" % (teamId)] += TANKS[DATA[_GAMEBLK]["version"]][tnk]["premium"]
        DATA[_BATRES]["gt%spremdmg" % (teamId)] += DATA[_RESBLK]["vehicles"][RPLID][0]["damageDealt"] if TANKS[DATA[_GAMEBLK]["version"]][tnk]["premium"] > 0 else 0
        # shots
        DATA[_BATRES]["gt%sshots" % (teamId)] += DATA[_RESBLK]["vehicles"][RPLID][0]["shots"]
        # hits
        DATA[_BATRES]["gt%shits" % (teamId)] += DATA[_RESBLK]["vehicles"][RPLID][0]["directHits"]
        DATA[_BATRES]["gt%shitsrec" % (teamId)] += DATA[_RESBLK]["vehicles"][RPLID][0]["directHitsReceived"]
        DATA[_BATRES]["gt%shehits" % (teamId)] += DATA[_RESBLK]["vehicles"][RPLID][0]["explosionHits"]
        DATA[_BATRES]["gt%shehitsrec" % (teamId)] += DATA[_RESBLK]["vehicles"][RPLID][0]["explosionHitsReceived"]
        DATA[_BATRES]["gt%stotalhits" % (teamId)] += (DATA[_RESBLK]["vehicles"][RPLID][0]["directHits"] + DATA[_RESBLK]["vehicles"][RPLID][0]["explosionHits"])
        # pens
        DATA[_BATRES]["gt%spens" % (teamId)] += DATA[_RESBLK]["vehicles"][RPLID][0]["piercings"]
        DATA[_BATRES]["gt%spensrec" % (teamId)] += DATA[_RESBLK]["vehicles"][RPLID][0]["piercingsReceived"]
        # mileage
        DATA[_BATRES]["gt%smileage" % (teamId)] += DATA[_RESBLK]["vehicles"][RPLID][0]["mileage"]
        # tanks damaged
        DATA[_BATRES]["gt%stanksdamaged" % (teamId)] += DATA[_RESBLK]["vehicles"][RPLID][0]["damaged"]
        # recochets (luck or experience)
        DATA[_BATRES]["gt%srico" % (teamId)] += DATA[_RESBLK]["vehicles"][RPLID][0]["noDamageDirectHitsReceived"]

    ### postprocessing
    # averages
    DATA[_BATRES]["gtavgxp"] = round(DATA[_BATRES]["gtavgxp"] / DATA[_BATRES]["gtplcnt"], 2)
    DATA[_BATRES]["gtavgdmg"] = round(DATA[_BATRES]["gtavgdmg"] / DATA[_BATRES]["gtplcnt"], 2)
    DATA[_BATRES]["gtouravgmileage"] = round(DATA[_BATRES]["gtourmileage"] / DATA[_BATRES]["gtplcnt"], 2)
    DATA[_BATRES]["gttheiravgmileage"] = round(DATA[_BATRES]["gttheirmileage"] / DATA[_BATRES]["gtplcnt"], 2)
    # tier diff
    DATA[_BATRES]["gtierdiff"] = DATA[_BATRES]["gtier"] - DATA[_BATRES]["gtbott"] if DATA[_BATRES]["gtier"] > DATA[_BATRES]["gtbott"] else DATA[_BATRES]["gtier"] - DATA[_BATRES]["gttopt"]
    # lifetime and survivors
    DATA[_BATRES]["gtouralive"] += DATA[_BATRES]["gtplcnt"]
    DATA[_BATRES]["gttheiralive"] += DATA[_BATRES]["gtplcnt"]
    DATA[_BATRES]["gtouravglifetime"] = round(DATA[_BATRES]["gtourlifetime"] / DATA[_BATRES]["gtplcnt"], 2)
    DATA[_BATRES]["gttheiravglifetime"] = round(DATA[_BATRES]["gttheirlifetime"] / DATA[_BATRES]["gtplcnt"], 2)
    # scorediff
    DATA[_BATRES]["gtscorediff"] = DATA[_BATRES]["gtouralive"] - DATA[_BATRES]["gttheiralive"]  # positive - we won, negative - they won
    # prem tanks
    DATA[_BATRES]["gtpremtankdiff"] = DATA[_BATRES]["gtourprems"] - DATA[_BATRES]["gttheirprems"]  # positive, we have more, negative otherwise

def calculateStatPercentages():
    ### percentage calculations
    # scorediff % & tierdiff %
    DATA[_BATRES]["gtscorediff%"] = round(DATA[_BATRES]["gtscorediff"] / DATA[_BATRES]["gtplcnt"], 2)
    DATA[_BATRES]["gtierdiff%"] = DATA[_BATRES]["gtierdiff"] / ARGS[_ABSMMSPR]  # this is to get %
    # my %
    DATA[_BATRES]["gxpteamxp%"] = round(DATA[_BATRES]["gxp"] / DATA[_BATRES]["gtourxp"], 2) if DATA[_BATRES]["gtourxp"] != 0 else 0
    DATA[_BATRES]["gdmgteamdmg%"] = round(DATA[_BATRES]["gtotaldmg"] / DATA[_BATRES]["gtourtotaldmg"], 2) if DATA[_BATRES]["gtourtotaldmg"] != 0 else 0
    DATA[_BATRES]["gdmgteamdmgassist%"] = round(DATA[_BATRES]["gassistdmg"] / DATA[_BATRES]["gtourassistdmg"], 2) if DATA[_BATRES]["gtourassistdmg"] != 0 else 0
    DATA[_BATRES]["ghitsshots%"] = round(DATA[_BATRES]["gtotalhits"] / DATA[_BATRES]["gshots"], 2) if DATA[_BATRES]["gshots"] != 0 else 0
    DATA[_BATRES]["gpenshits%"] = round(DATA[_BATRES]["gpens"] / DATA[_BATRES]["gtotalhits"], 2) if DATA[_BATRES]["gtotalhits"] != 0 else 0
    DATA[_BATRES]["gmileage%"] = round(DATA[_BATRES]["gmileage"] / DATA[_BATRES]["gtouravgmileage"], 2) if DATA[_BATRES]["gtouravgmileage"] != 0 else 0
    # lifetime diff %
    DATA[_BATRES]["gtlifetimediff%"] = round((DATA[_BATRES]["gtouravglifetime"] - DATA[_BATRES]["gttheiravglifetime"]) / max(DATA[_BATRES]["gtouravglifetime"], DATA[_BATRES]["gttheiravglifetime"]), 2) if max(DATA[_BATRES]["gtouravglifetime"], DATA[_BATRES]["gttheiravglifetime"]) != 0 else 0
    # prem tank diff %
    DATA[_BATRES]["gtpremtankdiff%"] = round(DATA[_BATRES]["gtpremtankdiff"] / max(DATA[_BATRES]["gtourprems"], DATA[_BATRES]["gttheirprems"]), 2) if max(DATA[_BATRES]["gtourprems"], DATA[_BATRES]["gttheirprems"]) != 0 else 0
    # prem tank dmg diff %
    DATA[_BATRES]["gtpremdmgdiff%"] = round((DATA[_BATRES]["gtourpremdmg"] - DATA[_BATRES]["gttheirpremdmg"]) / max(DATA[_BATRES]["gtourpremdmg"], DATA[_BATRES]["gttheirpremdmg"]), 2) if max(DATA[_BATRES]["gtourpremdmg"], DATA[_BATRES]["gttheirpremdmg"]) != 0 else 0
    # dmg diff %
    DATA[_BATRES]["gttotaldmgdiff%"] = round((DATA[_BATRES]["gtourtotaldmg"] - DATA[_BATRES]["gttheirtotaldmg"]) / max(DATA[_BATRES]["gtourtotaldmg"], DATA[_BATRES]["gttheirtotaldmg"]), 2) if max(DATA[_BATRES]["gtourtotaldmg"], DATA[_BATRES]["gttheirtotaldmg"]) != 0 else 0
    # hit %
    DATA[_BATRES]["gtourhitsshots%"] = round(DATA[_BATRES]["gtourhits"] / DATA[_BATRES]["gtourshots"], 2) if DATA[_BATRES]["gtourshots"] != 0 else 0
    DATA[_BATRES]["gttheirhitsshots%"] = round(DATA[_BATRES]["gttheirhits"] / DATA[_BATRES]["gttheirshots"], 2) if DATA[_BATRES]["gttheirshots"] != 0 else 0
    # pen %
    DATA[_BATRES]["gtourpenshits%"] = round(DATA[_BATRES]["gtourpens"] / DATA[_BATRES]["gtourtotalhits"], 2) if DATA[_BATRES]["gtourtotalhits"] != 0 else 0
    DATA[_BATRES]["gttheirpenshits%"] = round(DATA[_BATRES]["gttheirpens"] / DATA[_BATRES]["gttheirtotalhits"], 2) if DATA[_BATRES]["gttheirtotalhits"] != 0 else 0
    # opoosing teams hit %
    DATA[_BATRES]["gthitsshotsdiff%"] = round((DATA[_BATRES]["gtourhitsshots%"] - DATA[_BATRES]["gttheirhitsshots%"]) / max(DATA[_BATRES]["gtourhitsshots%"], DATA[_BATRES]["gttheirhitsshots%"]), 2) if max(DATA[_BATRES]["gtourhitsshots%"], DATA[_BATRES]["gttheirhitsshots%"]) != 0 else 0
    # opoosing teams pen %
    DATA[_BATRES]["gtpenshitsdiff%"] = round((DATA[_BATRES]["gtourpenshits%"] - DATA[_BATRES]["gttheirpenshits%"]) / max(DATA[_BATRES]["gtourpenshits%"], DATA[_BATRES]["gttheirpenshits%"]), 2) if max(DATA[_BATRES]["gtourpenshits%"], DATA[_BATRES]["gttheirpenshits%"]) != 0 else 0
    # opposing teams mileage diff %
    DATA[_BATRES]["gtmileagediff%"] = round((DATA[_BATRES]["gtourmileage"] - DATA[_BATRES]["gttheirmileage"]) / max(DATA[_BATRES]["gtourmileage"], DATA[_BATRES]["gttheirmileage"]), 2) if max(DATA[_BATRES]["gtourmileage"], DATA[_BATRES]["gttheirmileage"]) != 0 else 0

def adjustSessionStats():
    # global modifiers
    global DATAX

    # get battle date
    DATAX["gdate"] = datetime.datetime.strptime(DATA[_GAMEBLK]["dateTime"], "%d.%m.%Y %H:%M:%S")
    # this computes values in between the files that are processed at once
    if "gsessiondate" not in DATAX:
        # compute
        DATAX["gsessionnumber"] = 1
    elif (DATAX["gdate"] - DATAX["gsessiondate"]).total_seconds() > ARGS[_IDLESECS]:
        # compute
        DATAX["gsessionnumber"] = DATAX["gsessionnumber"] + 1 if "gsessionnumber" in DATAX else 1
    # adjust session date
    DATAX["gsessiondate"] = DATAX["gdate"]

    # adjust week number
    DATAX["gweeknumber"] = "%swk%s" % (DATAX["gdate"].strftime("%Y"), str(DATAX["gdate"].isocalendar()[1]).rjust(2, "0"))
    # adjust month
    DATAX["gmonthnumber"] = DATAX["gdate"].strftime("%Y%m")

def saveBattleResults():
    # file names
    # split files in week or month
    if ARGS[_REPSPLIT] is not None:
        st1fname = "%s/%s" % (ARGS[_FRESDIR], ("%s.stats.%s.csv" % (DATA[_GAMEBLK]["playerName"], DATAX["gweeknumber" if ARGS[_REPSPLIT] == "w" else "gmonthnumber"])))
    else:
        st1fname = "%s/%s" % (ARGS[_FRESDIR], "%s.stats.csv" % (DATA[_GAMEBLK]["playerName"]))
    # tank file names
    st2fname = "%s/%s" % (ARGS[_FRESDIR], "%s.stats.%s.csv" % (DATA[_GAMEBLK]["playerName"], TANKS[DATA[_GAMEBLK]["version"]][DATA[_PLINFO]["tankCompDescr"]]["nameNorm"]))
    # the time has come, we can finally save the stats
    st1ex = os.path.exists(st1fname)
    st2ex = os.path.exists(st2fname)

    # compose header (header, then format)
    head = "Battle date;Tank name;Tank type;Tank tier;Map"
    form = "%s;%s;%s;%i;%s"
    head = head + ";Session number;Battle len;Win;Score diff"
    form = form + ";%s;%i;%i;%i"
    head = head + ";My tier;Top tier;Bottom tier;Tier diff"
    form = form + ";%i;%i;%i;%i"
    head = head + ";Tier diff %;Score diff %;Hits/shots diff %;Pens/hits diff %;Mileage diff %"
    form = form + ";%f;%f;%f;%f;%f"
    head = head + ";Lifetime diff %;DMG diff %;Prem tank diff %;Prem tank DMG diff %"
    form = form + ";%f;%f;%f;%f"
    head = head + ";My/team XP %;My/team DMG %;My/team assist DMG %"
    form = form + ";%f;%f;%f"
    head = head + ";My hits/shots %;My pens/hits %;My/team avg mileage %"
    form = form + ";%f;%f;%f"
    head = head + ";My mileage;My tanks damaged;My XP;My kills;My death"
    form = form + ";%i;%i;%i;%i;%i"
    head = head + ";My spots;My lifetime;My DMG blocked;My shots;My pens;My hits"
    form = form + ";%i;%i;%i;%i;%i;%i"
    head = head + ";My HE hits;My hits received;My HE hits received;My no-DMG hits received"
    form = form + ";%i;%i;%i;%i"
    head = head + ";My direct DMG;My assist DMG;My total DMG;My DMG received;My pens received"
    form = form + ";%i;%i;%i;%i"
    head = head + ";Team AVG XP;Team AVG DMG"
    form = form + ";%i;%i"
    head = head + ";Team DMG;Team alive;Team AVG lifetime;Team prem tanks;Team prem tank DMG"
    form = form + ";%i;%i;%i;%i;%i"
    head = head + ";Team shots;Team hits;Team HE hits;Team pens"
    form = form + ";%i;%i;%i;%i"
    head = head + ";Team hits/shots %;Team pens/hits %;Team mileage;Team no-DMG hits received"
    form = form + ";%i;%i;%i;%i;%i"
    head = head + ";Enemy DMG;Enemy alive;Enemy AVG lifetime;Enemy prem tanks;Enemy prem tank DMG"
    form = form + ";%i;%i;%i;%i;%i"
    head = head + ";Enemy shots;Enemy hits;Enemy HE hits;Enemy pens"
    form = form + ";%i;%i;%i;%i"
    head = head + ";Enemy hits/shots %;Enemy pens/hits %;Enemy mileage;Enemy no-DMG hits received"
    form = form + ";%i;%i;%i;%i"
    head = head + "\n"
    form = form + "\n"

    # compose data
    data = form % (
        DATA[_BATRES]["gdate"], DATA[_BATRES]["gtank"], DATA[_BATRES]["gtanktype"], DATA[_BATRES]["gtier"], DATA[_BATRES]["mapName"],
        DATA[_BATRES]["gsessionnumber"], DATA[_BATRES]["gbattlelen"], DATA[_BATRES]["gwon"], DATA[_BATRES]["gtscorediff"],
        DATA[_BATRES]["gtier"], DATA[_BATRES]["gttopt"], DATA[_BATRES]["gtbott"], DATA[_BATRES]["gtierdiff"],
        DATA[_BATRES]["gtierdiff%"], DATA[_BATRES]["gtscorediff%"], DATA[_BATRES]["gthitsshotsdiff%"], DATA[_BATRES]["gtpenshitsdiff%"], DATA[_BATRES]["gtmileagediff%"],
        DATA[_BATRES]["gtlifetimediff%"], DATA[_BATRES]["gttotaldmgdiff%"], DATA[_BATRES]["gtpremtankdiff%"], DATA[_BATRES]["gtpremdmgdiff%"],
        DATA[_BATRES]["gxpteamxp%"], DATA[_BATRES]["gdmgteamdmg%"], DATA[_BATRES]["gdmgteamdmgassist%"],
        DATA[_BATRES]["ghitsshots%"], DATA[_BATRES]["gpenshits%"], DATA[_BATRES]["gmileage%"],
        DATA[_BATRES]["gmileage"], DATA[_BATRES]["gtanksdamaged"], DATA[_BATRES]["gxp"], DATA[_BATRES]["gkills"], DATA[_BATRES]["gdeath"],
        DATA[_BATRES]["gspots"], DATA[_BATRES]["glifetime"], DATA[_BATRES]["gblocked"], DATA[_BATRES]["gshots"], DATA[_BATRES]["gpens"], DATA[_BATRES]["ghits"],
        DATA[_BATRES]["ghehits"], DATA[_BATRES]["ghitsrec"], DATA[_BATRES]["ghehitsrec"], DATA[_BATRES]["gricochetsrec"],
        DATA[_BATRES]["gdmg"], DATA[_BATRES]["gassistdmg"], DATA[_BATRES]["gtotaldmg"], DATA[_BATRES]["gdmgrec"], DATA[_BATRES]["gpensrec"],
        DATA[_BATRES]["gtavgxp"], DATA[_BATRES]["gtavgdmg"],
        DATA[_BATRES]["gtourtotaldmg"], DATA[_BATRES]["gtouralive"], DATA[_BATRES]["gtouravglifetime"], DATA[_BATRES]["gtourprems"], DATA[_BATRES]["gtourpremdmg"],
        DATA[_BATRES]["gtourshots"], DATA[_BATRES]["gtourhits"], DATA[_BATRES]["gtourhehits"], DATA[_BATRES]["gtourpens"],
        DATA[_BATRES]["gtourhitsshots%"],DATA[_BATRES]["gtourpenshits%"], DATA[_BATRES]["gtourmileage"], DATA[_BATRES]["gtourrico"],
        DATA[_BATRES]["gttheirtotaldmg"], DATA[_BATRES]["gttheiralive"], DATA[_BATRES]["gttheiravglifetime"], DATA[_BATRES]["gttheirprems"], DATA[_BATRES]["gttheirpremdmg"],
        DATA[_BATRES]["gttheirshots"], DATA[_BATRES]["gttheirhits"], DATA[_BATRES]["gttheirhehits"], DATA[_BATRES]["gttheirpens"],
        DATA[_BATRES]["gttheirhitsshots%"],DATA[_BATRES]["gttheirpenshits%"], DATA[_BATRES]["gttheirmileage"], DATA[_BATRES]["gttheirrico"]
    )

    # write results to overall file
    with open(st1fname, "a") as fo:
        # if file did not exist, let's create a heading too
        if not st1ex:
            # write header row
            fo.write(head)
        # write out data
        fo.write(data)

    # only if have to
    if ARGS[_TANKF] > 0:
        # write results to overall file
        with open(st2fname, "a") as fi:
            # header
            if not st2ex:
                fi.write(head)
            # only if we need to
            fi.write(data)

def isint(pVal):
    # vars
    result = True
    try:
        if int(pVal) != 1.99:
            pass
    except Exception:
        result = False
    # return
    return(result)

# exec main method
if __name__ == '__main__':
    # vars
    i = -1
    # print help if needed
    if len(sys.argv) == 2 and sys.argv[1] == "--help":
        log(-99, "Usage:\n  python3 wotreplay2csv.py [--c n][--l n][--w n][--i n][--t n][--d x]")
        log(-99, "Options:\n  --c n (n is the max replays to process (default = 999999999999))")
        log(-99, "  --l n (n is the logging level, 0 - default, 1 - warning, -1 - nothing except fatal errors, 2 - debug)")
        log(-99, "  --w n (n is the 0 or 1, if n > 0, processing stops on important warnings (default = 1))")
        log(-99, "  --i n (n is the max number of seconds between the battles to be considered in one session (default = 3600))")
        log(-99, "  --t n (n is the 0 or 1, if n > 0, separate CSV will be generated for each tank you had fought (default = 0))")
        log(-99, "  --d x (x is w or m, this option splits result files in calendar weeks or months, it also disables --t option (default = none))")
        log(-99, "  --D x (x is directory where to look for replay files (default = replays))")
        exit(0)

    # parse arguments (the first has to to be the file name)
    for argument in sys.argv:
        # cnt
        i+=1;
        # check for file count
        if argument == "--c":
            # check if next arg exists and check if it is valid
            if len(sys.argv) < i+1 or not isint(sys.argv[i+1]):
                log(-99, 'ERROR: option --c specified incorrectly')
                sys.exit(2)
            # file count to process
            ARGS[_FCNT] = int(sys.argv[i+1])
        # check for logging level
        elif argument == "--l":
            # check if next arg exists and check if it is valid
            if len(sys.argv) < i+1 or not isint(sys.argv[i+1]):
                log(-99,"ERROR: option --l specified incorrectly")
                sys.exit(2)
            # file count to process
            ARGS[_LOGL] = int(sys.argv[i+1])
        # check for stop on important warning
        elif argument == "--w":
            # check if next arg exists and check if it is valid
            if len(sys.argv) < i+1 or not isint(sys.argv[i+1]):
                log(-99,"ERROR: option --w specified incorrectly")
                sys.exit(2)
            # file count to process
            ARGS[_WARNSTOP] = int(sys.argv[i+1])
        # check for active / idle time
        elif argument == "--i":
            # check if next arg exists and check if it is valid
            if len(sys.argv) < i+1 or not isint(sys.argv[i+1]):
                log(-99,"ERROR: option --i specified incorrectly")
                sys.exit(2)
            # file count to process
            ARGS[_IDLESECS] = int(sys.argv[i+1])
        # check for generate separate files for tanks
        elif argument == "--t":
            # check if next arg exists and check if it is valid
            if len(sys.argv) < i+1 or not isint(sys.argv[i+1]):
                log(-99,"ERROR: option --t specified incorrectly")
                sys.exit(2)
            # file count to process
            ARGS[_TANKF] = int(sys.argv[i+1])
        # check for generate separate files for tanks
        elif argument == "--d":
            # check if next arg exists and check if it is valid
            if len(sys.argv) < i+1 or not sys.argv[i+1] in ("m", "w"):
                log(-99,"ERROR: option --d specified incorrectly")
                sys.exit(2)
            # file count to process
            ARGS[_REPSPLIT] = sys.argv[i+1]
        # where to take the replay files
        elif argument == "--D":
            # check if next arg exists and check if it is valid
            if len(sys.argv) < i+1:
                log(-99,"ERROR: option --D specified incorrectly")
                sys.exit(2)
            # file count to process
            ARGS[_FREPDIR] = sys.argv[i+1]
        # ## there is no additonal arguments as of now ##

        # post argument corrections
        if ARGS[_REPSPLIT] is not None:
            # disable per tank file generation
            ARGS[_TANKF] = 0

    # check directories
    for RDIR in (ARGS[_FREPDIR], ARGS[_FTNKDIR], ARGS[_FRESDIR]):
        # error if directory not found
        if not os.path.isdir(RDIR):
            log(-99, "ERROR: directory \"%s\" does not exist" % (RDIR))
            sys.exit(2)

    # execute the main function for processing files
    main()
